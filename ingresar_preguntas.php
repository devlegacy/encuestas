<?php
error_reporting(0); //Quitar las alertas

	session_start();
	$varsesion = $_SESSION['usuario'];
	if($varsesion == null || $varsesion == ''){
		header('Location: mensajes/autorizacion.php');
		die();
	}
?>
<?php
include('consultas.php')
?>
<?php

// Realizar una consulta SQL
$sqlpregunta = "SELECT * FROM departamentos";
$sql6 = "SELECT *, @rownum := @rownum + 1 AS ID FROM preguntas2, (SELECT @rownum := 0) r";
$sql7 = "SELECT *, @rownum := @rownum + 1 AS ID10 FROM preguntas10, (SELECT @rownum := 0) r";


// Ejecutar comprobar si existe algun error
if (!$departamento_pregunta = $mysqli->query($sqlpregunta)) {
    echo "Error: La ejecución de la consulta falló debido a: \n";
    echo "Query: " . $sqlpregunta . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}

// Ejecutar comprobar si existe algun error
if (!$preguntas = $mysqli->query($sql6)) {
    echo "Error: La ejecución de la consulta falló debido a: \n";
    echo "Query: " . $sql6 . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}
// Ejecutar comprobar si existe algun error
if (!$preguntas10 = $mysqli->query($sql7)) {
    echo "Error: La ejecución de la consulta falló debido a: \n";
    echo "Query: " . $sql7 . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Agrega la pregunta deseada</title>
	<link rel="stylesheet" type="text/css" href="estilos/estilo_preguntas.css">
  <script type='text/javascript'>
function confirmDelete()
{
   return confirm("Estas seguro de Eliminar este Registro?");
}
</script>
</head>

<body>
	<div class="div_header">
		<header>
			<h1>Bienvenido <?php echo $_SESSION['usuario'] ?></h1>
			<a href="cerrar_sesion.php" class="cerrar_sesion">Cerrar Sesión</a>
		</header>
	</div>
	<form action="conexion_preguntas.php" method="post" class="form-register">
	<h2 class="form_titulo">Agregar preguntas</h2>
		<div class="contenedor-inputs">
  			<input type="text" name="pregunta" class="input_preguntas" placeholder="Ingresa la pregunta deseada" required>
  			<select name = "depto" class="select_dpto">
     <?php foreach ( $departamento_pregunta as $option ) : ?>
            <p><option name = "depto" value=<?php echo $option[departamento] ?>><?php echo $option['departamento']; ?></option></p>           
     <?php endforeach; ?>
  			</select>
  			<select name="estatus" class="select_estatus">
  				<option value="2">Por favor valore los siguientes atributos de Owen Group</option>
  				<option value="10">Como calificaria los conocimientos de su ejecutivo de cuenta, en los siguientes temas</option>
  			</select>	
		    	<input type="submit" value="Registrar Pregunta" id="input_boton" class="enviar">
		    	<a href="panel.php" value="Cancelar" id="input_boton" class="cancelar">Regresar</a>
    </form>
        <table>
    <thead>
      <tr>
        <th >ID</th>
        <th >Por favor valore los siguientes atributos de Owen Group</th>
        <th >Departamento</th>
        <th>Accion</th>
      </tr>
    </thead>
    <tbody>
     <?php foreach ( $preguntas as $preg ) : ?>
      <tr>
        <td><?php echo $preg[ID] ?></td>
        <td><?php echo $preg[pregunta] ?></td>
        <td><?php echo $preg[departamento] ?></td>
             <td>
               <form action="modificar_pregunta2.php" method="post">
                        <input type="hidden" name="pregunta2" value=<?php echo $preg[id_pregunta2] ?>></input>   
                        <input type="submit" value="Modificar" class="modificar" id="input_boton2">
               </form>
                <form action="delete_pregunta2.php" method="post">
                        <input type="hidden" name="pregunta2" value=<?php echo $preg[id_pregunta2] ?>></input>   
                        <input type="submit" value="Delete" class="delete" id="input_boton2" onclick='return confirmDelete()'>
                </form>
        </td>
      </tr>

     <?php endforeach; ?> 

         <thead>
      <tr>
        <th>ID</th>
        <th>Como calificaria los conocimientos de su ejecutivo de cuenta</th>
        <th>Departamento</th>
        <th width="200">Accion</th>
      </tr>
    </thead>
          <?php foreach ( $preguntas10 as $preg10 ) : ?>
      <tr>
        <td><?php echo $preg10[ID10] ?></td>
        <td><?php echo $preg10[pregunta] ?></td>
        <td><?php echo $preg10[departamento] ?></td>
           <td>
               <form action="modificar_pregunta10.php" method="post">
                        <input type="hidden" name="pregunta10" value=<?php echo $preg10[id_pregunta10] ?>></input>   
                        <input type="submit" value="Modificar" class="modificar" id="input_boton2">
               </form>
               <form action="delete_pregunta10.php" method="post">
                        <input type="hidden" name="pregunta10" value=<?php echo $preg10[id_pregunta10] ?>></input>   
                        <input type="submit" value="Delete" class="delete" id="input_boton2" onclick='return confirmDelete()'>
               </form>

        </td>
      </tr>
     <?php endforeach; ?> 
    </tbody>
  </table>
</body>
</html>
<?php

// Cerrar la conexión
$mysqli->close();
?>

