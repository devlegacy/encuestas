<?php

use App\Libraries\Core\View;

foreach($views as $view) {
  View::render($view, $data);
}
