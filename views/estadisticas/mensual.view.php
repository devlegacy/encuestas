<form id="frm-graphs-by-executive" action="" method="post">
  <div>
    <label for="from">Desde:</label> <input type="date" name="from" id="from" required>
  </div>
  <div>
    <label for="to">Hasta:</label>  <input type="date" name="to" id="to" required>
  </div>
  <div>
    <label for="executive">Ejecutivo:</label>
    <select name="executive" id="executive">
      <?php foreach ($executives as $executive): ?>
        <option value="<?= $executive['id_ejecutivo'] ?>"><?= $executive['ejecutivo'] ?></option>
      <?php endforeach ?>
    </select>
    <button type="submit">Generar gráficas</button>
  </div>
</form>
<div style="display:flex;">
  <div id="graphs-container" style="width:50%;">
    <h3>Gráficas:</h3>
  </div>
  <div id="comments-container" style="width:50%;">
    <h3>Comentarios:</h3>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
  const canvasTemplate = (id) => `<canvas id="${id}" width="800" height="450"></canvas>`;
  const commentParagraphTemplate = (comments) => {
    let paragraphHTML = '';
    comments.forEach(comment => paragraphHTML +=`<p>${comment}</p>`);
    return paragraphHTML;
  };
  const commentTemplate = (comment) => (`
    <div>
      <h4>${comment.title}</h4>
      ${commentParagraphTemplate(comment.comments)}
    </div>
  `);
  const graphsContainer = document.getElementById('graphs-container');
  const commentsContainer = document.getElementById('comments-container');
  const frmGraphsByCompany = document.getElementById('frm-graphs-by-executive');
  frmGraphsByCompany.addEventListener('submit', (e) => {
    e.preventDefault();
    const data = {
      from: e.target.from.value,
      to: e.target.to.value,
      company: e.target.company.value,
    };
    axios.post('/xhr/estadisticas/ejecutivos', data)
    .then(function (response) {
      // handle success
      const charts = response.data.charts || [];
      graphsContainer.innerHTML = '<h3>Gráficas:</h3>';
      commentsContainer.innerHTML = '<h3>Comentarios:</h3>';
      charts.forEach(chart => {
        graphsContainer.insertAdjacentHTML('beforeend', canvasTemplate(chart.canvas));
      });
      return response;
    })
    .then(function(response){
      const charts = response.data.charts;
      const comments = response.data.comments;

      charts.forEach(chart => {
        const ctx = document.getElementById(chart.canvas).getContext('2d');
        new Chart(ctx, {
            type: 'pie',
            data: {
              labels: chart.labels,
              datasets: [{
                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#1D3461"],
                data: chart.data
              }]
            },
            options: {
              title: {
                display: true,
                text: chart.title,
              },
              onAnimationComplete: function() {
                  this.showTooltip(this.segments, true);
              },
              showTooltips: true,
            }
        })
      });

      comments.forEach(comment => {
        commentsContainer.insertAdjacentHTML('beforeend', commentTemplate(comment));
      });

    })
    .catch(function (error) {
      // handle error
      console.log(error);
    });
  });

</script>
