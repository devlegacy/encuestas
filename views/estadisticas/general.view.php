<div class="container">
  <div class="graphs-container">
    <div class="form-container">
      <h3>Generador de gráficas por fecha</h3>
      <form id="frm-graphs-by-company" action="" method="post">
        <fieldset>
          <legend>Fechas</legend>
          <div>
            <label for="from">Desde:</label> <input class="input" type="date" name="from" id="from" value="<?= $from ?>" required>
          </div>
          <div>
            <label for="to">Hasta:</label>  <input class="input" type="date" name="to" id="to" value="<?= $to ?>" required>
          </div>
        </fieldset>
        <fieldset>
          <legend>Empresa</legend>
          <div>
            <label for="company">Seleccione una empresa:</label>
            <select class="input" name="company" id="company">
                <option value="all">Todas las empresas</option>
                <?php foreach ($companies as $company): ?>
                  <option value="<?= $company['id_cliente'] ?>"><?= $company['cliente'] ?></option>
                <?php endforeach ?>
            </select>
          </div>
        </fieldset>

        <input type="hidden" name="total-questions" value="<?= $totalQuestions ?>">
        <?php foreach ($groupQuestions as $questions): ?>
        <fieldset>
          <?php if (!empty($questions['title'])): ?>
          <legend><?= $questions['title'] ?></legend>
          <?php endif ?>
          <?php foreach ($questions['questions'] as $question): ?>
          <div class="questions__option">
            <input type="checkbox" name="questions[]" id="questions" value="<?= $question['id'] ?>" checked/> <?= $question['question'] ?>
          </div>
          <?php endforeach ?>
        </fieldset>
        <?php endforeach ?>

        <button class="btn btn__primary " type="submit">
          Generar gráficas
          <div class="btn-bouncing-loader">
            <div class="">
              <div></div>
              <div></div>
              <div></div>
            </div>
          </div>
        </button>
      </form>
    </div>
    <div class="form-container">
      <h3>Generador de gráficas por mes</h3>
      <form id="frm-graphs-by-company-and-month" action="" method="post">
        <fieldset>
          <legend>Fecha</legend>
          <div>
            <label for="month">Mes:</label>
            <input type="month" class="input" name="month" id="month" value="<?= date('Y-m')?>">
          </div>
        </fieldset>

        <input type="hidden" name="total-questions" value="<?= $totalQuestions ?>">
        <?php foreach ($groupQuestions as $questions): ?>
        <fieldset>
          <?php if (!empty($questions['title'])): ?>
          <legend><?= $questions['title'] ?></legend>
          <?php endif ?>
          <?php foreach ($questions['questions']  as $question): ?>
          <div class="questions__option">
            <input type="checkbox" name="questions[]" id="questions" value="<?= $question['id'] ?>" checked/> <?= $question['question'] ?>
          </div>
          <?php endforeach ?>
        </fieldset>
        <?php endforeach ?>

        <button class="btn btn__primary " type="submit">
          Generar gráficas
          <div class="btn-bouncing-loader">
            <div class="">
              <div></div>
              <div></div>
              <div></div>
            </div>
          </div>
        </button>
      </form>
    </div>
  </div>

  <div id="executive-container"class="flex-center-column" >
    <h3>Ejecutivos</h3>
  </div>
  <div style="display:flex;">
    <div id="graphs-container" style="width:50%;">
      <h3>Gráficas:</h3>
    </div>
    <div id="comments-container" style="width:50%;">
      <h3>Comentarios:</h3>
    </div>
  </div>
</div>
