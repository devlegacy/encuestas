<?php

namespace App\Controllers\Xhr;

use App\Models\Company;
use App\Models\Executive;
use App\Models\Encuesta;
use Carbon\Carbon;

class EstadisticasController
{
    public function general()
    {
        header('Content-type:application/json;charset=utf-8');
        $_POST = json_decode(file_get_contents("php://input"), true);
        $company = new Company();
        $encuesta = new Encuesta();
        $client = $_POST['company'];
        $from = $_POST['from'];
        $to = $_POST['to'];
        $questions = $_POST['questions'];
        $totalQuestions = $_POST['totalCuestions'];
        if ($client === 'all') {
            $estadisticas = $company->estadisticasCountFrom($from, $to, $questions, $totalQuestions);
        } else {
            $estadisticas = $company->estadisticasFrom($client, $from, $to, $questions, $totalQuestions);
            $estadisticas->executives =$encuesta->getExecutiveBy($client, $from, $to);
        }
        echo json_encode($estadisticas);
    }

    public function mensual()
    {
        header('Content-type:application/json;charset=utf-8');

        $_POST = json_decode(file_get_contents("php://input"), true);
        $company = new Company();
        $encuesta = new Encuesta();
        $dt = new \DateTime("first day of {$_POST['month']}");
        $carbon = Carbon::instance($dt);
        $from = $carbon->format('Y-m-d');
        $dt = new \DateTime("last day of {$_POST['month']}");
        $carbon = Carbon::instance($dt);
        $to = $carbon->format('Y-m-d');
        $questions = $_POST['questions'];
        $totalQuestions = $_POST['totalCuestions'];

        $estadisticas = $company->estadisticasCountFrom($from, $to, $questions, $totalQuestions);
        echo json_encode($estadisticas);

        /**
         * Example of response
         *

            echo json_decode(json_encode(
                '

              {
                "charts": [
                    {
                        "title": "Calidad en el servicio",
                        "canvas": "calidad-en-el-servicio",
                        "labels": [
                            [
                                "Bueno",
                                "Wifi Omartz Caribe: 1",
                                "Taquerias Omartz:  1"
                            ],
                            [
                                "Malo",
                                "Wifi Omartz Caribe: 1"
                            ]
                        ],
                        "data": [
                            2,
                            1
                        ],
                        "pre": {
                            "bueno": 2,
                            "malo": 1
                        },
                        "aux": {
                            "bueno": [
                                "Wifi Omartz Caribe: 1",
                                "Taquerias Omartz:  1"
                            ],
                            "malo": [
                                "Wifi Omartz Caribe: 1"
                            ]
                        },
                        "aux-name": {
                            "bueno": "Bueno",
                            "malo": "Malo"
                        }
                    },
                    {
                        "title": "Nóminas  (ISR - SAE)",
                        "canvas": "nominas-isr-sae",
                        "labels": [
                            [
                                "Regular"
                            ],
                            [
                                "Muy malo"
                            ],
                            [
                                "Excelente"
                            ],
                            [
                                "Wifi Omartz Caribe: 1"
                            ],
                            [
                                "Taquerias Omartz: 1"
                            ],
                            [
                                "Wifi Omartz Caribe: 1"
                            ]
                        ],
                        "data": [
                            1,
                            1,
                            1
                        ],
                        "pre": {
                            "excelente": 1,
                            "muy-malo": 1,
                            "regular": 1
                        },
                        "aux": {
                            "excelente": [
                                "Wifi Omartz Caribe: 1"
                            ],
                            "muy-malo": [
                                "Taquerias Omartz: 1"
                            ],
                            "regular": [
                                "Wifi Omartz Caribe: 1"
                            ]
                        },
                        "aux-name": {
                            "excelente": "Excelente",
                            "muy-malo": "Muy malo",
                            "regular": "Regular"
                        }
                    },
                    {
                        "title": "Infonavit - IMSS",
                        "canvas": "infonavit-imss",
                        "labels": [
                            [
                                "Regular"
                            ],
                            [
                                "Malo"
                            ],
                            [
                                "Bueno"
                            ],
                            [
                                "Wifi Omartz Caribe: 1"
                            ],
                            [
                                "Taquerias Omartz: 1"
                            ],
                            [
                                "Wifi Omartz Caribe: 1"
                            ]
                        ],
                        "data": [
                            1,
                            1,
                            1
                        ],
                        "pre": {
                            "bueno": 1,
                            "malo": 1,
                            "regular": 1
                        },
                        "aux": {
                            "bueno": [
                                "Wifi Omartz Caribe: 1"
                            ],
                            "malo": [
                                "Taquerias Omartz: 1"
                            ],
                            "regular": [
                                "Wifi Omartz Caribe: 1"
                            ]
                        },
                        "aux-name": {
                            "bueno": "Bueno",
                            "malo": "Malo",
                            "regular": "Regular"
                        }
                    },
                    {
                        "title": "Temas jurídicos laborales",
                        "canvas": "temas-juridicos-laborales",
                        "labels": [
                            [
                                "Regular"
                            ],
                            [
                                "Malo"
                            ],
                            [
                                "Wifi Omartz Caribe: 1"
                            ],
                            [
                                "Wifi Omartz Caribe: 1",
                                "Taquerias Omartz:  1"
                            ]
                        ],
                        "data": [
                            1,
                            2
                        ],
                        "pre": {
                            "malo": 1,
                            "regular": 2
                        },
                        "aux": {
                            "malo": [
                                "Wifi Omartz Caribe: 1"
                            ],
                            "regular": [
                                "Wifi Omartz Caribe: 1",
                                "Taquerias Omartz:  1"
                            ]
                        },
                        "aux-name": {
                            "malo": "Malo",
                            "regular": "Regular"
                        }
                    },
                    {
                        "title": "Facturación (IVA)",
                        "canvas": "facturacion-iva",
                        "labels": [
                            [
                                "Regular"
                            ],
                            [
                                "Malo"
                            ],
                            [
                                "Bueno"
                            ],
                            [
                                "Taquerias Omartz: 1"
                            ],
                            [
                                "Wifi Omartz Caribe: 1"
                            ],
                            [
                                "Wifi Omartz Caribe: 1"
                            ]
                        ],
                        "data": [
                            1,
                            1,
                            1
                        ],
                        "pre": {
                            "bueno": 1,
                            "malo": 1,
                            "regular": 1
                        },
                        "aux": {
                            "bueno": [
                                "Taquerias Omartz: 1"
                            ],
                            "malo": [
                                "Wifi Omartz Caribe: 1"
                            ],
                            "regular": [
                                "Wifi Omartz Caribe: 1"
                            ]
                        },
                        "aux-name": {
                            "bueno": "Bueno",
                            "malo": "Malo",
                            "regular": "Regular"
                        }
                    },
                    {
                        "title": "Tiempo de respuesta",
                        "canvas": "tiempo-de-respuesta",
                        "labels": [
                            [
                                "Regular"
                            ],
                            [
                                "Wifi Omartz Caribe: 2",
                                "Taquerias Omartz:  1"
                            ]
                        ],
                        "data": [
                            3
                        ],
                        "pre": {
                            "regular": 3
                        },
                        "aux": {
                            "regular": [
                                "Wifi Omartz Caribe: 2",
                                "Taquerias Omartz:  1"
                            ]
                        },
                        "aux-name": {
                            "regular": "Regular"
                        }
                    },
                    {
                        "title": "Atención al Cliente",
                        "canvas": "atencion-al-cliente",
                        "labels": [
                            [
                                "Malo"
                            ],
                            [
                                "Bueno"
                            ],
                            [
                                "Wifi Omartz Caribe: 1"
                            ],
                            [
                                "Wifi Omartz Caribe: 1",
                                "Taquerias Omartz:  1"
                            ]
                        ],
                        "data": [
                            1,
                            2
                        ],
                        "pre": {
                            "bueno": 1,
                            "malo": 2
                        },
                        "aux": {
                            "bueno": [
                                "Wifi Omartz Caribe: 1"
                            ],
                            "malo": [
                                "Wifi Omartz Caribe: 1",
                                "Taquerias Omartz:  1"
                            ]
                        },
                        "aux-name": {
                            "bueno": "Bueno",
                            "malo": "Malo"
                        }
                    },
                    {
                        "title": "¿Recibe el apoyo necesario cuando se le presenta un problema y/o duda?",
                        "canvas": "recibe-el-apoyo-necesario-cuando-se-le-presenta-un-problema-y-o-duda",
                        "labels": [
                            [
                                "si"
                            ],
                            [
                                "no"
                            ],
                            [
                                "Taquerias Omartz: 1"
                            ],
                            [
                                "Wifi Omartz Caribe: 2"
                            ]
                        ],
                        "data": [
                            1,
                            2
                        ],
                        "pre": {
                            "no": 1,
                            "si": 2
                        },
                        "aux": {
                            "no": [
                                "Taquerias Omartz: 1"
                            ],
                            "si": [
                                "Wifi Omartz Caribe: 2"
                            ]
                        },
                        "aux-name": {
                            "no": "no",
                            "si": "si"
                        }
                    },
                    {
                        "title": "¿Cómo conoció a Owen Group?",
                        "canvas": "como-conocio-a-owen-group",
                        "labels": [
                            [
                                "Amigos, colegas o contactos"
                            ],
                            [
                                "Wifi Omartz Caribe: 2",
                                "Taquerias Omartz:  1"
                            ]
                        ],
                        "data": [
                            3
                        ],
                        "pre": {
                            "amigos-colegas-o-contactos": 3
                        },
                        "aux": {
                            "amigos-colegas-o-contactos": [
                                "Wifi Omartz Caribe: 2",
                                "Taquerias Omartz:  1"
                            ]
                        },
                        "aux-name": {
                            "amigos-colegas-o-contactos": "Amigos, colegas o contactos"
                        }
                    },
                    {
                        "title": "¿Cómo califica el servicio de Owen Group en general?",
                        "canvas": "como-califica-el-servicio-de-owen-group-en-general",
                        "labels": [
                            [
                                "Muy malo"
                            ],
                            [
                                "Malo"
                            ],
                            [
                                "Excelente"
                            ],
                            [
                                "Taquerias Omartz: 1"
                            ],
                            [
                                "Wifi Omartz Caribe: 1"
                            ],
                            [
                                "Wifi Omartz Caribe: 1"
                            ]
                        ],
                        "data": [
                            1,
                            1,
                            1
                        ],
                        "pre": {
                            "excelente": 1,
                            "malo": 1,
                            "muy-malo": 1
                        },
                        "aux": {
                            "excelente": [
                                "Taquerias Omartz: 1"
                            ],
                            "malo": [
                                "Wifi Omartz Caribe: 1"
                            ],
                            "muy-malo": [
                                "Wifi Omartz Caribe: 1"
                            ]
                        },
                        "aux-name": {
                            "excelente": "Excelente",
                            "malo": "Malo",
                            "muy-malo": "Muy malo"
                        }
                    },
                    {
                        "title": "¿Recomendaría nuestros servicios a otras empresas?",
                        "canvas": "recomendaria-nuestros-servicios-a-otras-empresas",
                        "labels": [
                            [
                                "Probablemente"
                            ],
                            [
                                "No"
                            ],
                            [
                                "Definitivamente"
                            ],
                            [
                                "Wifi Omartz Caribe: 1"
                            ],
                            [
                                "Taquerias Omartz: 1"
                            ],
                            [
                                "Wifi Omartz Caribe: 1"
                            ]
                        ],
                        "data": [
                            1,
                            1,
                            1
                        ],
                        "pre": {
                            "definitivamente": 1,
                            "no": 1,
                            "probablemente": 1
                        },
                        "aux": {
                            "definitivamente": [
                                "Wifi Omartz Caribe: 1"
                            ],
                            "no": [
                                "Taquerias Omartz: 1"
                            ],
                            "probablemente": [
                                "Wifi Omartz Caribe: 1"
                            ]
                        },
                        "aux-name": {
                            "definitivamente": "Definitivamente",
                            "no": "No",
                            "probablemente": "Probablemente"
                        }
                    },
                    {
                        "title": "¿Cuánto tiempo lleva utilizando nuestros servicios?",
                        "canvas": "cuanto-tiempo-lleva-utilizando-nuestros-servicios",
                        "labels": [
                            [
                                "0 - 6 meses"
                            ],
                            [
                                "Wifi Omartz Caribe: 2",
                                "Taquerias Omartz:  1"
                            ]
                        ],
                        "data": [
                            3
                        ],
                        "pre": {
                            "0-6-meses": 3
                        },
                        "aux": {
                            "0-6-meses": [
                                "Wifi Omartz Caribe: 2",
                                "Taquerias Omartz:  1"
                            ]
                        },
                        "aux-name": {
                            "0-6-meses": "0 - 6 meses"
                        }
                    }
                ],
                "comments": [

                ]
              }
              '
            ));
         */
    }
}
