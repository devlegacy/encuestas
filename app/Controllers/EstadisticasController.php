<?php

namespace App\Controllers;

use App\Libraries\Core\View;
use App\Models\Company;
use App\Models\Encuesta;
use Carbon\Carbon;

class EstadisticasController
{
    public function general()
    {
        $companies = Company::all();
        $encuesta = new Encuesta();
        $questions = $encuesta->getQuestions();

        $firstRow = $encuesta->filterQuestionsById($questions, ['conocio','tiempo']);
        sort($firstRow);
        $secondRowServiceQualification = $encuesta->filterQuestionsById($questions, ['pregunta1']);
        $thirdRowQualificationIn = $encuesta->filterQuestionsById($questions, [1, 2, 3]);
        $fourthRowExecutiveQualification = $encuesta->filterQuestionsById($questions, ['1.1', '1.2', '1.3','1.4']);
        $fifthRowSupport = $encuesta->filterQuestionsById($questions, ['apoyo']);
        $sixthComments = $encuesta->filterQuestionsById($questions, ['comentarios','comentarios1','comentarios2']);
        sort($sixthComments);
        $seventhRowRecommend = $encuesta->filterQuestionsById($questions, ['recomendacion']);
        $groupQuestions = [
          [
            'title' => '',
            'questions'=>$firstRow,
          ],
          [
            'title' => '',
            'questions'=> $secondRowServiceQualification,
          ],
          [
            'title' => 'Qué calificación le daría a Owen Group, en:',
            'questions'=> $thirdRowQualificationIn,
          ],
          [
            'title' => 'Cómo calificaría los conocimientos de su ejecutivo de cuenta, en los siguientes temas:',
            'questions'=> $fourthRowExecutiveQualification,
          ],
          [
            'title' => '',
            'questions'=> $fifthRowSupport,
          ],
          [
            'title' => '',
            'questions'=> $sixthComments,
          ],
          [
            'title' => '',
            'questions'=> $seventhRowRecommend,
          ],
        ];
        $totalQuestions = count($questions);
        $dt = new \DateTime("first day of this month");
        $carbon = Carbon::instance($dt);
        $from = $carbon->format('Y-m-d');
        $dt = new \DateTime("last day of this month");
        $carbon = Carbon::instance($dt);
        $to = $carbon->format('Y-m-d');
        $data = [
          'views' => [
            'templates/header',
            'estadisticas/general',
            'templates/footer'
          ],
          'data' => [
            'title' => 'Gráficas general',
            'companies' => $companies,
            'questions' => $questions,
            'groupQuestions' => $groupQuestions,
            'totalQuestions' => $totalQuestions,
            'from' => $from,
            'to' => $to,
          ]
        ];
        View::render('templates/template', $data);
    }
}
