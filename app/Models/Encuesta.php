<?php
namespace App\Models;

use App\Libraries\Core\BaseModel;

class Encuesta extends BaseModel
{
    protected static $table = 'encuestas';
    public function __construct()
    {
        parent::__construct();
    }

    public function getExecutiveBy($company, $from, $to)
    {
        $executives = $this->db->query(" SELECT ejecutivos.ejecutivo FROM ".self::$table."
                                              LEFT JOIN ejecutivos ON encuestas.id_ejecutivo = ejecutivos.id_ejecutivo
                                              WHERE  encuestas.id_empresa='{$company}' AND (fecha BETWEEN '{$from}' AND '{$to}')
                                              GROUP BY ejecutivos.id_ejecutivo");
        return $executives;
    }

    public function getQuestions()
    {
        $q1 = $this->db->query("SELECT preguntas2.id_pregunta2 as id, preguntas2.pregunta as question FROM  `preguntas2`");
        $q2 = $this->db->query("SELECT preguntas10.id_secundario as id, preguntas10.pregunta as question FROM `preguntas10`");
        $q3 = [
          ['id'=>'tiempo' , 'question' => '¿Cuánto tiempo lleva utilizando nuestros servicios?'],
          ['id'=>'conocio', 'question' => '¿Cómo conoció a Owen Group?'],
          ['id'=>'pregunta1', 'question' => '¿Cómo califica el servicio de Owen Group en general?'],
          ['id'=>'apoyo', 'question' => '¿Recibe el apoyo necesario cuando se le presenta un problema y/o duda?'],
          ['id'=>'recomendacion', 'question' => '¿Recomendaría nuestros servicios a otras empresas?'],
          ['id'=>'comentarios1', 'question' => 'Coméntenos ¿Qué le gusta de nuestro servicio?'],
          ['id'=>'comentarios2', 'question' => 'Coméntenos ¿Qué NO le gusta de nuestro servicio?'],
          ['id'=>'comentarios', 'question' => 'Coméntenos sobre su experiencia'],
        ];
        $questions = array_merge($q1, $q2, $q3);
        ksort($questions);
        return $questions;
    }

    public function filterQuestionsById($questions, $filter)
    {
        return array_filter($questions, function ($question) use ($filter) {
            return in_array($question['id'], $filter);
        });
    }
}
