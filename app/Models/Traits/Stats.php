<?php

namespace App\Models\Traits;

trait Stats
{
    public function extractComments($estadisticas)
    {
        $extractedComments = [];
        foreach ($estadisticas as $key => $estadistica) {
            if (stripos($estadistica['id'], 'comentario') !== false) {
                array_push($extractedComments, $estadisticas[$key]);
                unset($estadisticas[$key]);
            }
        }
        return [$estadisticas, $extractedComments];
    }

    public function formatComments($extractedComments)
    {
        $_temp = [];
        foreach ($extractedComments as $key => $comment) {
            if (!array_key_exists($comment['id'], $_temp)) {
                $_temp[$comment['id']] = ['title' => '', 'comments' => []];
            }
            $_temp[$comment['id']]['title'] = $comment['question'];
            array_push($_temp[$comment['id']]['comments'], $comment['responses']);
        }
        $comments = array_values($_temp);
        return $comments;
    }

    public function formatCommentsWithExecutivesAndClients($extractedComments)
    {
        $_temp = [];
        foreach ($extractedComments as $key => $comment) {
            if (!array_key_exists($comment['id'], $_temp)) {
                $_temp[$comment['id']] = ['title' => '', 'comments' => []];
            }
            $_temp[$comment['id']]['title'] = $comment['question'];
            array_push(
                $_temp[$comment['id']]['comments'],
                "<p><a href='ingresar_ejecutivo.php'><strong>Ejecutivo:</strong></a>{$comment['ejecutivo']}</p>
                 <p><a href='/ingresar_cliente.php'><strong>Cliente:</strong></a> {$comment['cliente']}</p>
                 <p>{$comment['responses']}</p>"
            );
        }
        $comments = array_values($_temp);
        return $comments;
    }

    public function formatStatistics($estadisticas)
    {
        $_temp = [];
        foreach ($estadisticas as $key => $estadistica) {
            if (!array_key_exists($estadistica['id'], $_temp)) {
                $_temp[$estadistica['id']] = ['title' => '', 'canvas' => '', 'labels' => [], 'data' => []];
            }
            $_temp[$estadistica['id']]['title'] = $estadistica['question'];
            $_temp[$estadistica['id']]['id'] = $estadistica['id'];
            $_temp[$estadistica['id']]['canvas'] = slugify($estadistica['question']);
            array_push($_temp[$estadistica['id']]['labels'], $estadistica['responses']);
            array_push($_temp[$estadistica['id']]['data'], $estadistica['count']);
        }
        $charts = array_values($_temp);
        return $charts;
    }

    public function groupStats($charts)
    {
        $firstRow = $this->filterQuestionsById($charts, ['conocio','tiempo']);
        sort($firstRow);
        // var_dump($firstRow);
        $secondRowServiceQualification = $this->filterQuestionsById($charts, ['pregunta1']);
        // var_dump($secondRowServiceQualification);
        $thirdRowQualificationIn = $this->filterQuestionsById($charts, [1, 2, 3]);
        $fourthRowExecutiveQualification = $this->filterQuestionsById($charts, ['1_1', '1_2', '1_3','1_4']);
        $fifthRowSupport = $this->filterQuestionsById($charts, ['apoyo']);
        $sixthRecommend = $this->filterQuestionsById($charts, ['recomendacion']);
        $groupCharts = [
          [
            'title' => '',
            'questions'=>array_values($firstRow),
          ],
          [
            'title' => '',
            'questions'=> array_values($secondRowServiceQualification),
          ],
          [
            'title' => 'Qué calificación le daría a Owen Group, en:',
            'questions'=> array_values($thirdRowQualificationIn),
          ],
          [
            'title' => 'Cómo calificaría los conocimientos de su ejecutivo de cuenta, en los siguientes temas:',
            'questions'=> array_values($fourthRowExecutiveQualification),
          ],
          [
            'title' => '',
            'questions'=> array_values($fifthRowSupport),
          ],
          [
            'title' => '',
            'questions'=> array_values($sixthRecommend),
          ],
        ];
        // die();
        return $groupCharts;
    }

    public function filterQuestionsById($questions, $filter)
    {
        return array_filter($questions, function ($question) use ($filter) {
            return in_array($question['id'], $filter, true);
        });
    }
}
