<?php
namespace App\Models;

use App\Libraries\Core\BaseModel;

class Executive extends BaseModel
{
    protected static $table = 'ejecutivos';

    public function __construct()
    {
        parent::__construct();
    }

    public static function all()
    {
        $model = new BaseModel();
        $companies = $model->query("SELECT id_ejecutivo, ejecutivo FROM ".self::$table);
        return $companies;
    }
}
