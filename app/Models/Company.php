<?php
namespace App\Models;

use App\Libraries\Core\BaseModel;
use App\Models\Traits\Stats;

class Company extends BaseModel
{
    use Stats;
    protected static $table = 'clientes';

    public function __construct()
    {
        parent::__construct();
    }

    public static function all()
    {
        $model = new BaseModel();
        $companies = $model->query("SELECT cliente, id_ejecutivo, id_cliente FROM ".self::$table);
        return $companies;
    }

    public function estadisticasFrom($company, $from, $to, $questions = [], $totalQuestions = 0)
    {
        $id = null;
        $filterQuery = '';
        if ((count($questions) != $totalQuestions)) {
            foreach ($questions as $question) {
                $question = str_replace(".", "_", $question);
                $filterQuery .= " encuestas.id LIKE '$question' ||";
            }
            $filterQuery = " AND (".rtrim($filterQuery, '||').")";
        }

        $filterCompany = '';
        if ($company) {
            $filterCompany = "encuestas.id_empresa='{$company}' AND";
        }
        // Full query
        $estadisticas = $this->db->plainQuery("
                          SELECT
                            id,
                            respuestas,
                            COUNT(respuestas) AS count,
                            CONCAT(IF(question IS NULL, '', question), IF(question_2 IS NULL, '', question_2), IF(question_3 IS NULL, '', question_3) ) AS question,
                            CONCAT(
                                    IF(antiguedad IS NULL, '', antiguedad),
                                    IF(entero IS NULL, '', entero),
                                    IF(satisfaction IS NULL, '',
                                    IF(antiguedad IS NOT NULL OR entero IS NOT NULL,'',satisfaction)),
                                    IF(respuestas IN('si','no','Definitivamente', 'Probablemente'),respuestas,''),
                                    IF(id IN('comentarios', 'comentarios1', 'comentarios2'),  respuestas, '')) as responses

                          FROM (

                                SELECT
                                  encuestas.id,
                                  encuestas.respuestas,
                                  antiguedad.antiguedad ,
                                  entero.entero,
                                  IF(encuestas.respuestas = 1, 'Muy malo',
                                  IF(encuestas.respuestas = 2, 'Malo',
                                  IF(encuestas.respuestas = 3, 'Regular',
                                  IF(encuestas.respuestas = 4, 'Bueno',
                                  IF(encuestas.respuestas = 5, 'Excelente', null))))) as satisfaction,
                                  IF(encuestas.id = 'tiempo', '¿Cuánto tiempo lleva utilizando nuestros servicios?',
                                  IF(encuestas.id = 'conocio', '¿Cómo conoció a Owen Group?',
                                  IF(encuestas.id = 'pregunta1', '¿Cómo califica el servicio de Owen Group en general?',
                                  IF(encuestas.id = 'apoyo', '¿Recibe el apoyo necesario cuando se le presenta un problema y/o duda?',
                                  IF(encuestas.id = 'recomendacion','¿Recomendaría nuestros servicios a otras empresas?',
                                  IF(encuestas.id = 'comentarios1','Coméntenos ¿Qué le gusta de nuestro servicio?',
                                  IF(encuestas.id = 'comentarios2','Coméntenos ¿Qué NO le gusta de nuestro servicio?',
                                  IF(encuestas.id = 'comentarios','Coméntenos sobre su experiencia',null)))))))) as question,
                                  preguntas2.pregunta AS question_2,
                                  preguntas10.pregunta AS question_3
                              FROM `encuestas`
                              LEFT JOIN antiguedad ON encuestas.respuestas = antiguedad.id_antiguedad AND encuestas.id = 'tiempo'
                              LEFT JOIN entero ON encuestas.respuestas = entero.id_entero AND encuestas.id = 'conocio'
                              LEFT JOIN preguntas2 ON encuestas.id = preguntas2.id_pregunta2 AND encuestas.id LIKE '_'
                              LEFT JOIN preguntas10 ON REPLACE(encuestas.id,'_','.') = preguntas10.id_secundario
                              WHERE $filterCompany (fecha BETWEEN '{$from}' AND '{$to}') {$filterQuery}
                            ) AS questions
                            GROUP BY id, respuestas");
        // Separate comments
        list($estadisticas, $extractedComments) = $this->extractComments($estadisticas);

        $comments = $this->formatComments($extractedComments);
        $charts = $this->formatStatistics($estadisticas);
        $groupCharts = $this->groupStats($charts);

        $response = new \stdClass();
        $response->charts = $groupCharts;
        $response->comments = $comments;
        return $response;
    }

    public function estadisticasCountFrom($from, $to, $questions = [], $totalQuestions = 0)
    {
        $filterQuery = '';
        if ((count($questions) != $totalQuestions)) {
            foreach ($questions as $question) {
                $question = str_replace(".", "_", $question);
                $filterQuery .= " encuestas.id LIKE '$question' ||";
            }
            $filterQuery = " AND (".rtrim($filterQuery, '||').")";
        }
        $estadisticas = $this->db->plainQuery("
                        SELECT
                                id,
                                COUNT(respuestas) AS respuestas,
                                cliente,
                                cliente_id,
                                ejecutivo,
                                CONCAT(IF(question IS NULL, '', question), IF(question_2 IS NULL, '', question_2), IF(question_3 IS NULL, '', question_3) ) AS question,
                                CONCAT(
                                        IF(antiguedad IS NULL, '', antiguedad),
                                        IF(entero IS NULL, '', entero),
                                        IF(satisfaction IS NULL, '',
                                        IF(antiguedad IS NOT NULL OR entero IS NOT NULL,'',satisfaction)),
                                        IF(respuestas IN('si','no','Definitivamente', 'Probablemente'),respuestas,''),
                                        IF(id IN('comentarios', 'comentarios1', 'comentarios2'),  respuestas, '')) as responses
                        FROM (
                              SELECT
                                encuestas.id,
                                encuestas.respuestas,
                                antiguedad.antiguedad ,
                                entero.entero,
                                IF(encuestas.respuestas = 1, 'Muy malo',
                                IF(encuestas.respuestas = 2, 'Malo',
                                IF(encuestas.respuestas = 3, 'Regular',
                                IF(encuestas.respuestas = 4, 'Bueno',
                                IF(encuestas.respuestas = 5, 'Excelente', null))))) as satisfaction,
                                IF(encuestas.id = 'tiempo', '¿Cuánto tiempo lleva utilizando nuestros servicios?',
                                IF(encuestas.id = 'conocio', '¿Cómo conoció a Owen Group?',
                                IF(encuestas.id = 'pregunta1', '¿Cómo califica el servicio de Owen Group en general?',
                                IF(encuestas.id = 'apoyo', '¿Recibe el apoyo necesario cuando se le presenta un problema y/o duda?',
                                IF(encuestas.id = 'recomendacion','¿Recomendaría nuestros servicios a otras empresas?',
                                IF(encuestas.id = 'comentarios1','Coméntenos ¿Qué le gusta de nuestro servicio?',
                                IF(encuestas.id = 'comentarios2','Coméntenos ¿Qué NO le gusta de nuestro servicio?',
                                IF(encuestas.id = 'comentarios','Coméntenos sobre su experiencia',null)))))))) as question,
                                preguntas2.pregunta AS question_2,
                                preguntas10.pregunta AS question_3,
                                clientes.cliente AS cliente,
                                clientes.id_cliente AS cliente_id,
                                ejecutivos.ejecutivo
                            FROM `encuestas`
                            LEFT JOIN antiguedad ON encuestas.respuestas = antiguedad.id_antiguedad AND encuestas.id = 'tiempo'
                            LEFT JOIN entero ON encuestas.respuestas = entero.id_entero AND encuestas.id = 'conocio'
                            LEFT JOIN preguntas2 ON encuestas.id = preguntas2.id_pregunta2 AND encuestas.id LIKE '_'
                            LEFT JOIN preguntas10 ON REPLACE(encuestas.id,'_','.') = preguntas10.id_secundario
                            LEFT JOIN clientes ON encuestas.id_empresa = clientes.id_cliente
                            LEFT JOIN ejecutivos ON clientes.id_ejecutivo = ejecutivos.id_ejecutivo
                            WHERE (fecha BETWEEN '{$from}' AND '{$to}') {$filterQuery}
                          ) AS questions
                          GROUP BY id, respuestas, cliente_id
                          ORDER BY id, responses, cliente_id
                      ");
        // Separate comments
        list($estadisticas, $extractedComments) = $this->extractComments($estadisticas);


        $_temp = [];
        foreach ($estadisticas as $key => $estadistica) {
            if (!array_key_exists($estadistica['id'], $_temp)) {
                $_temp[$estadistica['id']] = ['title' => '', 'canvas' => '', 'labels' => [], 'data' => [], 'pre' => [], 'aux' => [], 'aux-name' => []];
            }
            $_temp[$estadistica['id']]['title'] = $estadistica['question'];
            $_temp[$estadistica['id']]['canvas'] = slugify($estadistica['question']);
            // var_dump($estadistica['cliente_id']);
            $_temp_key = slugify($estadistica['responses']);
            if (!array_key_exists($_temp_key, $_temp[$estadistica['id']]['aux'])) {
                $_temp[$estadistica['id']]['aux'][$_temp_key] = [ "{$estadistica['cliente']}: {$estadistica['respuestas']}"];
            } else {
                array_push($_temp[$estadistica['id']]['aux'][$_temp_key], "{$estadistica['cliente']}:  {$estadistica['respuestas']}");
            }

            if (!array_key_exists($_temp_key, $_temp[$estadistica['id']]['pre'])) {
                $_temp[$estadistica['id']]['pre'][$_temp_key] =  $estadistica['respuestas'];
            } else {
                $_temp[$estadistica['id']]['pre'][$_temp_key] +=  $estadistica['respuestas'];
            }

            if (!array_key_exists($_temp_key, $_temp[$estadistica['id']]['aux-name'])) {
                $_temp[$estadistica['id']]['aux-name'][$_temp_key] =  $estadistica['responses'];
            }
            // array_push($_temp[$estadistica['id']]['labels'], $estadistica['responses']);
            // $_temp[$estadistica['id']]['data'] = array_count_values($_temp[$estadistica['id']]['labels']);
            // var_dump(["{$estadistica['cliente']}: {$estadistica['responses']}"]);
        }

        foreach ($_temp as $key => $chart) {
            foreach ($chart['pre'] as  $preKey => $pre) {
                array_push($_temp[$key]['data'], $pre);
            }
            foreach ($chart['aux-name'] as $auxNameKey => $auxName) {
                // echo $_temp[$key]['aux-name'][$auxNameKey];
                $_temp[$key]['aux-name'][$auxNameKey] = [
                  $_temp[$key]['aux-name'][$auxNameKey],
                ];
                foreach ($_temp[$key]['aux'][$auxNameKey] as $aux) {
                    array_push($_temp[$key]['aux-name'][$auxNameKey], $aux);
                }
            }
            $_temp[$key]['id']=$key;
            $_temp[$key]['labels'] = array_values($_temp[$key]['aux-name']);
        }

        $charts = array_values($_temp);
        $groupCharts = $this->groupStats($charts);
        $comments = $this->formatCommentsWithExecutivesAndClients($extractedComments);

        $response = new \stdClass();
        $response->charts = $groupCharts;
        $response->comments = $comments;
        return $response;
    }
}
