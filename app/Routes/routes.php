<?php
namespace App\Routes;

use App\Libraries\Core\Route;

// Route::any('encuestas','index');
// Route::any('estadisticas', 'estadisticas', 'index');
Route::any('xhr/estadisticas/general', 'xhr\estadisticas', 'general');
Route::any('xhr/estadisticas/mensual', 'xhr\estadisticas', 'mensual');
Route::any('estadisticas/general', 'estadisticas', 'general');
Route::any('estadisticas/mensual', 'estadisticas', 'mensual');
