<?php
namespace App\Libraries\Database;

use PDO;

class DB extends PDO
{
    private $hostname;
    private $database;
    private $username;
    private $password;
    private $pdo;
    private $stringQuery;
    private $dbConnected;
    private $parameters;

    public function __construct()
    {
        $this->initData();
        $this->connect();
        $this->parameters = [];
    }

    private function initData()
    {
        $this->hostname = getenv('DB_HOST') ? getenv('DB_HOST') : 'localhost';
        $this->database = getenv('DB_NAME') ? getenv('DB_NAME') : 'owengroup2018';
        $this->username = getenv('DB_USER') ? getenv('DB_USER') : 'root';
        $this->password = getenv('DB_PASSWORD') ? getenv('DB_PASSWORD') : '';
    }

    private function connect()
    {
        $this->initData();
        $dns =  "mysql:dbname={$this->database};host={$this->hostname};charset=utf8";
        try {
            $this->pdo = new PDO($dns, $this->username, $this->password, [
              //For PHP 5.3.6 or lower
              // PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
              PDO::ATTR_EMULATE_PREPARES => false,
              PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
              // PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true
            ]);
            $this->dbConnected = true;
        } catch (Exception $e) {
            echo $e->getMessage();
            die('connect');
        }
    }

    public function closeConnection()
    {
        $this->pdo = null;
    }

    private function initQuery($query, $parameters)
    {
        if (!$this->dbConnected) {
            $this->connect();
        }
        try {
            $this->parameters = $parameters;
            $this->stringQuery = $this->pdo->prepare($this->buildParams($query, $this->parameters));
            if (!empty($this->parameters)) {
                if (array_key_exists(0, $parameters)) {
                    $parametersType = true;
                    array_unshift($this->parameters, '');
                    unset($this->parameters[0]);
                } else {
                    $parametersType = false;
                }
                foreach ($this->parameters as $column => $value) {
                    $this->stringQuery->bindParam($parametersType ? intval($column) : ":" . $column, $this->parameters[$column]); //It would be query after loop end(before 'stringQuery->execute()').It is wrong to use $value.
                }
            }
            $this->success = $this->stringQuery->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            die(' initQuery');
        }

        $this->parameters = [];
    }

    private function buildParams($query, $params = [])
    {
        if (!empty($params)) {
            $arrayParameterFound = false;
            foreach ($params as $keyParam => $parameter) {
                if (is_array($parameter)) {
                    $arrayParameterFound = true;
                    $in = '';
                    foreach ($parameter as $key => $value) {
                        $namePlaceholder = "{$keyParam}_{$key}";
                        // concatenates params as named placeholders
                        $in .= ":{$namePlaceholder},\x7F";
                        // adds each single parameter to $params
                        $params[$namePlaceholder] = utf8_encode($value);
                    }
                    $in = rtrim($in, ', ');
                    $query = preg_replace("/:{$keyParam}/", $in, $query);
                    // removes array form $params
                    unset($params[$keyParam]);
                }
            }
            // updates $this->params if $params and $query have changed
            if ($arrayParameterFound) {
                $this->parameters = $params;
            }
        }
        return $query;
    }

    public function query($query, $params=null, $fetchmode = PDO::FETCH_ASSOC)
    {
        $query = trim($query);
        $rawStatement = explode(' ', $query);
        $this->initQuery($query, $params);
        $statement = strtolower($rawStatement[0]);
        if ($statement === 'select' || $statement === 'show') {
            return $this->stringQuery->fetchAll($fetchmode);
        } elseif ($statement === 'insert' || $statement === 'update' || $statement === 'delete') {
            return $this->stringQuery->rowCount();
        } else {
            return null;
        }
    }

    public function plainQuery($query, $fetchmode = PDO::FETCH_ASSOC)
    {
        return $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
    }
}
