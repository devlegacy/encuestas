<?php

namespace App\Libraries\Core;

class Route
{
    private static $routes =[];
    public static function any($route = null, $controller='', $action = 'index')
    {
        $controllerFilename = explode('\\', $controller);
        $controller = '';
        foreach ($controllerFilename as $con) {
            $controller.= ucfirst($con).'\\';
        }
        $controller = rtrim($controller, '\\');

        $controller = "\\App\\Controllers\\{$controller}Controller";


        self::$routes[$route] = ['file' => $controller , 'action' => $action];
    }

    public static function routing($route = null)
    {
        if (!array_key_exists($route, self::$routes)) {
            return false;
        }
        $controller = new self::$routes[$route]['file']();
        $action  = self::$routes[$route]['action'];
        if (!method_exists($controller, $action)) {
            header('HTTP/1.0 404 Not Found');
            die('Page no found');
        }

        $controller->$action();
        return true;
    }
}
