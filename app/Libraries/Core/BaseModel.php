<?php
namespace App\Libraries\Core;

use App\Libraries\Database\DB;

class BaseModel extends DB
{
    public $db;

    public function __construct()
    {
        $this->db = new DB();
    }
}
