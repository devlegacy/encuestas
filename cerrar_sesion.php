<?php
	session_start();
	$varsesion = $_SESSION['usuario'];
	if($varsesion == null || $varsesion == ''){
		header('Location: mensajes/autorizacion.php');
		die();
	}

	session_destroy();
	header('Location: login.php');
?>