-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 16, 2018 at 05:10 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `owengroup`
--

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` varchar(15) NOT NULL,
  `cliente` varchar(100) NOT NULL,
  `id_ejecutivo` int(2) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `razon_social` varchar(200) NOT NULL,
  `id_contacto` varchar(200) NOT NULL,
  `link` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `cliente`, `id_ejecutivo`, `correo`, `razon_social`, `id_contacto`, `link`) VALUES
('001/002', 'TUKAN', 5, '', '', '', ''),
('003-NUB', 'NUBWARE', 7, 'color@nubware.com', 'NUBWARE S.C.', '', ''),
('006-AME', 'AMESA CANCUN', 5, 'gerenciaadmon_amesaqroo@hotmail.com', ' AMESA DE QUINTANA ROO SA DE CV ', '', ''),
('011-CLE', 'CANCUN LOCAL EXPERT', 3, 'marco@cancunvacationexperts.com', 'CANCUN LOCAL EXPERT S.A. DE C.V.', '', ''),
('013-QSU', 'QUIRURGICA DEL SUR DR MADERA', 6, 'recepcion2dopiso@hotmail.com', 'MADERA GAMBOA JOEL MARTIN', '', ''),
('016-ODE', 'OCEAN DENTAL', 4, 'facturas@oceandentalcancun.com', ' OCEAN DENTAL S DE RL DE CV ', '', ''),
('018-VSI', 'VIDRIOS Y SIMILARES', 6, 'vidriosysimilares@hotmail.com', ' SERGIO GARZA LAURENT  ', '', ''),
('019-HFL', 'HOTEL FLAMINGOS', 4, 'adrian@hotelflamingo.com', ' LA JOLLA PROPERTIES S. DE R.L. C.V. ', '', ''),
('020-CDL', 'COPY LOBO', 3, 'copy_loboventas@hotmail.com', ' COPIADORAS DIGITALES LOBO S.A. DE C.V. ', '', ''),
('021-ABO', 'ALFONSO BOLIO ', 4, 'albolio@hotmail.com', 'ALFONSO BOLIO GOMEZ', '', ''),
('025-BZD', 'BALZADI', 8, 'victor.zapata@balzadi.com.mx', 'COMERCIALIZADORA BALZADI S.A. DE C.V.', 'Victor Zapata', ''),
('027MAQ', 'MAQUITEXA', 4, 'consuelo_monuc@hotmail.com', ' TEXTIYUC S.A. DE C.V ', '', ''),
('033-MDG', 'MEDIA GRUOP', 3, 'sbalam@mediagroup.com.mx', ' GRUPO INVESTI S.A. DE C.V. ', '', ''),
('037-ETW', ' EMERALD ', 2, 'asist.admon@emeraldtower.com.mx', ' ASOCIACION DE RESIDENTES  DE EMERALD RESIDENTIAL  TOWERS &  SPA  AC  ', '', ''),
('040-HPR', 'PAVO REAL', 6, 'rh_pavorealresorts@outlook.com', ' OPERADORA EROGI SA DE CV ', '', ''),
('041-MBA', 'MONEY BAR', 4, 'angelcalam@gmail.com', ' COMER REALTY INVESTMENTS S DE RL DE CV ', '', ''),
('047-SGR', 'SMART GREEN ', 3, 'gdavalos@smartgreen.com.mx', 'ARACELI GARCIA LUNA ARAICO', '', ''),
('049-HZM', 'HOTEL ZAMAS ', 6, 'mara@zamas.com', ' LA ZAMA VIDA SA DE CV ', '', ''),
('058-WAR', 'WINGS ARMY', 2, 'wingscancun@hotmail.com', ' OPERADORA ROSPED SA DE CV ', '', ''),
('062-MSB', 'EAT GREEN', 6, 'mau_corral@hotmail.com', ' EAT GREEN GRUPO RESTAURANTERO ', '', ''),
('063-INE', 'ALMA ROSA ', 3, 'npsalmarosamp@yahoo.com.mx', ' ALMA ROSA MARTINEZ PEREZ ', '', ''),
('064-LUX', 'LABORATORIO UXMAL', 4, 'laboratoriouxmal@hotmail.com', ' PACHÓN CETINA FERNANDO HUMBERTO ', '', ''),
('079-TER', 'SOLUCIONES INTEGRALES', 7, 'chuyterrazas@me.com', ' SOLUCIONES INTEGRALES ESTRATEGICAS T&S S DE RL DE CV ', '', ''),
('082-ZBR', 'ZEBRA', 7, 'abavetsanluis@hotmail.com', ' LUIS GONZALO CASTANEDO CAZARES ', '', ''),
('083-LUM', 'LUMEN', 4, 'titi_dargence@hotmail.com', ' MARÍA DEL CARMEN PÉREZ D\'ARGENCE. ', '', ''),
('084-ARB', 'ARBIBE ', 3, '', ' EDIFICACIONES ARBIBE S.A. DE C.V. ', '', ''),
('085-MCC', 'MARINA COCOTOURS', 5, 'dlfuente_2001@yahoo.com.mx', ' OPERADORA ACUATICA  DEPORTIVA DE VALLARTA SA DE CV ', '', ''),
('086-BET', 'AVABET', 7, 'abavetsanluis@hotmail.com', ' ABASTECEDORA VETERINARIA DE SAN LUIS S. DE R.L. DE C.V ', '', ''),
('089-BVR', 'BEVERAGE', 3, 'sediazc@prodigy.net.mx', ' BEVERAGE ENGINEERING  SA DE CV  ', '', ''),
('093-GHC', 'GRAND CITY HOTEL', 2, 'gcity.glopez@yahoo.com.mx', 'PSI SOLUCIONES S.A. de C.V.', '', ''),
('105-ANE', 'ANECON', 7, 'SSALDANA@ANECON.COM.MX', ' ANECON S.A. DE C.V. ', '', ''),
('110-FAU', 'FAUDEM', 7, 'Jzapata@faudem.com.mx', ' FAUDEM S.A. DE C.V. ', '', ''),
('112-UGL', 'UNIGLOBAL', 3, 'loredop@uniglobal-logistica.com', ' UNIGLOBAL LOGISTICA SA DE CV ', '', ''),
('119-CMO', 'EL CAMELLUCO ', 3, 'camelluco1@yahoo.com', ' EL CAMELLUCO S.A. DE C.V ', '', ''),
('121-PET', 'ALL PETS', 7, 'luis_azv@live.com.mx', ' COMERCIALIZADORA ALL PETS S. DE R.L. DE C.V. ', '', ''),
('122-COY', 'CONVEYOR SYSTEM', 8, 'alejandro.diaz@conveyorsystems.com.mx', ' CONVEYOR SYSTEMS S.A. de C.V. ', '', ''),
('123-FEI', ' FABRICA DE EQUIPOS INDSUTRIALES ', 8, 'alejandro.diaz@conveyorsystems.com.mx', ' FABRICA DE EQUIPOS INDUSTRIALES Y MANUFACTURAS S DE RL DE CV ', '', ''),
('124-PET', 'ALL PETS', 7, 'luis_azv@live.com.mx', ' COMERCIALIZADORA ALL PETS S. DE R.L. DE C.V. ', '', ''),
('129-COY', 'CONVEYOR SYSTEM ASIMILADOS ', 8, 'alejandro.diaz@conveyorsystems.com.mx', ' CONVEYOR SYSTEMS S.A. de C.V. ', '', ''),
('134-ALE', 'ANGLO LATINO EDUCATION', 7, 'julietagallegos316@gmail.com', ' ANGLO LATINO EDUCATION PARTNERSHIP LIMITED ', '', ''),
('139-MTD', 'MADERAS Y TABLEROES', 7, 'mleautaud@prodigy.net.mx', ' MADERAS Y TABLEROS DE SAN LUIS SA DE CV ', '', ''),
('142-MYK', 'MYKOMIR /PHOTOVENTURA', 5, 'administracion@bluelenscaribe.com', ' MYKOMIR SA DE CV ', '', ''),
('147-FCS', 'SIMARTEC ', 3, 'jesse@simartec.com.mx', 'FABRICACIONES Y COMPONENTES INDUSTRIALES SIMARTEC S.A. DE C.V', '', ''),
('161-RQU', 'RESIDENCIAL LAS QUINTAS', 4, 'administracion@lasquintas.org', ' MARIA LUISA TORRES RINCON GALLARDO ', '', ''),
('168-ODE', 'A-ONE DENTAL', 4, 'facturas@oceandentalcancun.com', ' A- ONE DENTAL PRODUCTS S.A. DE C.V. ', '', ''),
('173-FEF', 'LIMPIEZA GERARDO ', 2, 'zandzea@hotmail.com', ' FRANCISCO JAVIER FERNANDEZ MACIAS ', '', ''),
('184-APO', 'ROLANDIS', 2, '', 'ASOSCIACION DE PERFILES', '', ''),
('185-SZP', 'SANZPONT', 6, 'ARQUITECTURA@SANZPONT.COM', ' SP ARQUITECTURA Y ASOCIADOS S.A. DE C.V. ', '', ''),
('186-CRO', 'ROFELE', 6, 'luisfelipearetia@hotmail.com', ' COMERCIALIZADORA ROFELE S.A. DE C.V. ', '', ''),
('187-BIR', 'EFIPACK', 2, 'efipackfinanzas@hotmail.com', 'GINNA LETICIA BRISEÑO  RODRIGUEZ ', '', ''),
('189-MOC', 'APRENDERE', 8, 'smontante@libreriaaprendere.com.mx', ' OSCAR MONTANTE  CONTRERAS  ', '', ''),
('190-OST', 'HOTEL LOS LIRIOS', 6, 'conchacastillo77@hotmail.com', 'OPERADORA DE SERVICIOS TURISTICOS  E INMOBILIARIOS SA DE CV ', '', ''),
('198-MEP', 'MEDLAP ', 4, '', ' MEDLAPP  SA DE CV  ', '', ''),
('199-PIC', 'PREMEZCLADOS ', 7, 'papemo@hotmail.com', ' PREMEZCLADOS INDUSTRIALES DEL CENTRO SA DE CV ', '', ''),
('201-OGA', 'GRAN CENTRAL ', 7, 'gianinavillegas@gmail.com', 'OPERADORA GASTRONOMICA  AGUA BLANCA  S.A.P.I DE CV', '', ''),
('210-GUP', 'TRADUCTORES BEGUER ', 2, 'libaju61@gmail.com', ' FERNANDO GUERRERO Y PONCE  ', '', ''),
('220-RUR', 'SIECA', 7, 'ruzmex@hotmail.com', 'SIECA SOLUCIONES  Y SERVICIOS S D RL DE CV', '', ''),
('223-PFS', 'PENINSULA FREIGHT', 3, 'unitedgsa1@gmail.com', ' PENINSULA FREIGHT SERVICES SA DE CV  ', '', ''),
('225-ANA', 'GOOFELLAS', 3, 'malulu0385@hotmail.com', ' LA AUTENTICA NAPOLITANA SA DE CV  ', '', ''),
('229-ROM', 'TALLER IMPERIAL ', 3, '', ' JUANA RODRIGUEZ MOTA  ', '', ''),
('230-LOG', 'DIVERTY KIDS', 7, 'divertysanluis@hotmail.com', ' CAROLINA DE JESUS  LOPEZ GUZMAN  ', '', ''),
('231-PID', 'VITZO ', 6, '', ' PIDOMEX SA DE CV  ', '', ''),
('232-MCH', 'MAR CHARBEL HOSPITAL ', 8, 'dmadrigalmedina@gmail.com', ' MAR CHARBEL HOSPITAL SC  ', '', ''),
('235-MCS', 'MAR CHARBEL ADMTVO', 8, 'dmadrigalmedina@gmail.com', ' MAR CHARBEL HOSPITAL SC  ', '', ''),
('238-MCX', 'MAR CHARBEL ADMTVO', 8, 'dmadrigalmedina@gmail.com', ' MAR CHARBEL HOSPITAL SC  ', '', ''),
('239-MRO', 'MAYA ROMA', 5, 'paolorocco@hotmail.com', ' MAYA ROMA SA DE CV  ', '', ''),
('241-MRQ', 'MAYA ROMA', 5, 'paolorocco@hotmail.com', ' MAYA ROMA SA DE CV  ', '', ''),
('242-APB', 'AMIGO PACO  ', 3, 'gaby@amigopaco.com', ' AMIGO PACO  BIENES RAICES  SA DE CV  ', '', ''),
('243-CAX', 'ANA MEZCAL ', 4, 'boabogados@gmail.com', ' COMERCIALIZADORA ANA MEZCAL  S DE RL DE CV  ', '', ''),
('246-DTS', 'CANCUN DENTAL  SPECIALIST', 4, 'facturas@cancundentalspecialist.com', ' CANCUN DENTAL SPECIALIST  S DE RL DE CV ', '', ''),
('247-ABT', 'ALFONSO BOLIO  TRADICIONAL ', 4, 'albolio@hotmail.com', ' ALFONSO BOLIO GOMEZ ', '', ''),
('264-TDF', 'TALLER DE DECORACIONES FLORAL', 2, 'tallerdedecoracionfloral@outlook.com', ' TALLER DE DECORACION FLORAL SA DE CV ', '', ''),
('266', 'ARBIBE ASIMILADOS', 3, '', '', '', ''),
('269-OEC', 'ELIPARK CHURUBUSCO ', 5, 'roberto@elipark.com', ' OPERADORA ELISA S.A DE C.V ', '', ''),
('270-OEC', 'ELIPARK COACALCO ', 5, 'roberto@elipark.com', ' OPERADORA ELISA S.A DE C.V ', '', ''),
('271-OEP', 'ELIPARK PACHUCA ', 5, 'roberto@elipark.com', ' OPERADORA ELISA S.A DE C.V ', '', ''),
('273-IAR', 'INSTALACIONES ARBIBE ', 3, 'garguelles@arbibe.com.mx', ' INSTALACIONES ARBIBE S.A. DE C.V. ', '', ''),
('277-OEN', 'ELIPARK CANCUN ', 5, 'roberto@elipark.com', ' OPERADORA ELISA S.A DE C.V ', '', ''),
('278-OEZ', 'ELIPARK COLIMA ', 5, 'roberto@elipark.com', ' OPERADORA ELISA S.A DE C.V ', '', ''),
('280-OED', 'ELIPARK CD CARMEN ', 5, 'roberto@elipark.com', ' OPERADORA ELISA S.A DE C.V ', '', ''),
('286-MEQ', 'JORGE UGALDE ', 3, 'yorchbrand@gmail.com', ' JORGE MERCADO UGALDE ', '', ''),
('288-EXP', 'EXPORTODD', 8, 'exportodd@yahoo.com', ' EXPORTODD S.A DE C.V. ', '', ''),
('291-MEC', 'ROSA MARIA CAMPUZANO ', 3, 'encurtidos.mena@gmail.com', ' ROSA MARIA MENDEZ CAMPUZANO  ', '', ''),
('296-FCR', 'FAST CANCUN ', 2, 'fastcancun.contabilidad@gmail.com', ' FAST CANCUN CAR RENTAL SA DE CV  ', '', ''),
('301-ORI', 'OPERADORA RICKY', 5, 'rhumanoscancun@elipark.com', ' OPERADORA RICKY S.A DE C.V ', '', ''),
('302-SSM', 'SUMINISTROS Y SOLUCIONES', 8, 'madadisa@outlook.com', ' SUMINISTROS Y SOLUCIONES EN METAL  S DE RL DE CV  ', '', ''),
('308-TSO', 'TRANSPORTADORA SOLARE ', 6, 'ventas1@concordcarrental.com.mx', ' TRANSPORTADORA SOLARE S.A. DE C.V. ', '', ''),
('309-GVI', 'GORDO VIAJES ', 3, 'servicio@sumotransportes.com', ' GORDO VIAJES S.A. DE C.V. ', '', ''),
('313-ORY', 'ELIPARK PLAYA DEL CARMEN ', 5, 'roberto@elipark.com', ' OPERADORA ELISA S.A DE C.V ', '', ''),
('316-CPS', 'DELBIEN ', 8, 'rene2195@hotmail.com', ' COMERCIALIZADORA DE PRODUCTOS Y SERVICIOS DELBIEN S.A. DE C.V. ', '', ''),
('320-CRI', 'CAFÉ RIVERA ', 6, 'caferiviera@gmail.com', ' CAFÉ RIVERA SA DE CV  ', '', ''),
('323-ODS', 'SEGORBE', 7, '', ' OPERADORA DEPORTIVA SEGORBE SA DE CV ', '', ''),
('324-ODQ', 'SEGORBE', 7, '', ' OPERADORA DEPORTIVA SEGORBE SA DE CV ', '', ''),
('329-MEM', 'MEINEKE', 5, 'alfmeling@yahoo.com', ' EUGENIO ALF MELING  MIJARES  ', '', ''),
('331-GAA', 'GALAN Y ASOCIADO', 7, 'administracion@galan.mx', ' GALAN & ASOCIADO DE SAN LUIS POTOSI SC  ', '', ''),
('333-ARH', 'AUTO REPUESTOS DE HULE ', 8, 'admon_arh@hotmail.com', ' AUTO REPUESTOS DE HULE SA DE CV  ', '', ''),
('334-AUG', 'ALFONSO AGUILAR ', 2, 'aguilar_nadia04@hotmail.com', ' ALFONSO ANSELMO AGUILAR GONZALEZ ', '', ''),
('337-MSS', 'MULTISERVICIOS ', 3, '', ' MULTISERVICIOS SSELREH SA DE CV  ', '', ''),
('338-GAF', 'GESTION AMBIENTAL ', 7, '', ' GESTION AMBIENTAL FORESTAL E HIDRAULICA S.C.  ', '', ''),
('340-SBI', 'GOLDEN FRICTION ', 8, '', ' SUMINISTROS BASICOS INDUSTRIALES  SA DE CV ', '', ''),
('342-SBI', 'GOLDEN FRICTION ', 8, '', ' SUMINISTROS BASICOS INDUSTRIALES  SA DE CV ', '', ''),
('343-RME', 'REFACCIONES MEDINA ', 7, 'nominas-medina@hotmail.com', ' REFACCIONES MEDINA SA DE CV  ', '', ''),
('344-TPR', 'TECNOMECANICA ', 3, 'servicontfisc@hotmail.com', ' TECNOMECANICA DE PRECISION S DE RL DE CV ', '', ''),
('345-MVA', 'MARTINA KING', 4, '', ' MARTINA DEL ROSARIO KING PARK ', '', ''),
('347-IMA', 'IMACU', 4, 'icuenca@imacupro.com', ' IMACU S.A. DE C.V.  ', '', ''),
('348-TAT', 'ARQTELL', 4, 'arqtell@prodigy.net.mx', ' ARQTELL SC  // TELLEZ & TELLEZ ARQUITECTOS SA DE CV ', '', ''),
('354-CPS', 'COMERCIALIZADORA PERCAM', 4, 'conyil@hotmail.com', ' COMERCIALIZADORA PERCAM DEL SURESTE SA DE CV  ', '', ''),
('355-GAF', 'GESTION AMBIENTAL ', 7, 'lulanoemi_gr@hotmail.com', ' GESTION AMBIENTAL FORESTAL E HIDRAULICA S.C.  ', '', ''),
('359-MBA', 'COMER REALITY', 4, 'angelcalam@gmail.com', ' COMER REALTY INVESTMENTS S DE RL DE CV ', '', ''),
('360-MEX', 'MEXCLUSIVE ', 6, 'jorgelacrusc@gmail.com', ' MEXCLUSIVE S. DE R.L. DE C.V.  ', '', ''),
('361-IAR', 'INSTALACIONES ARBIBE  ASIMILADO', 3, '', ' INSTALACIONES ARBIBE  SA DE CV  ', '', ''),
('362-ARX', 'ARBIBE ASMILADOS ', 3, '', ' INSTALACIONES ARBIBE  SA DE CV  ', '', ''),
('372-AUG', 'ALFONSO AGUILAR ', 2, 'aguilar_nadia04@hotmail.com', ' ALFONSO ANSELMO AGUILAR GONZALEZ ', '', ''),
('373-GOC', 'SOCIAL EVENTS', 3, 'liz@socialeevents.com', ' ANA PATRICIA GOMEZ CEVALLOS  ', '', ''),
('382-HEX', 'ALEJANDRO HERNANDEZ || FLORERIA DALIA ', 2, 'dalia-floreria.admon@hotmail.com', ' ALEJANDRO HERNANDEZ DOMINGUEZ  ', '', ''),
('385-GOR', 'SARA PATRICIA ', 8, 'ventasmares_slp@hotmail.com', ' SARA PATRICIA GONZALEZ RODRIGUEZ  ', '', ''),
('387-HCD', 'POK TA POK', 3, 'rhumanos@cancungolfclub.com', ' HAZAMA CORPORATION DESARROLLO DE TURISMO SA DE CV ', '', ''),
('388-EAS', 'EARTH SOLUTIONS', 6, 'kira@earthandsolutions.com', ' EARTH & SOLUTIONS SC ', '', ''),
('389-GAR', 'MOTEL TORITO ', 2, 'fanny_silva10@hotmail.com', ' LUIS JOAQUÍN GARCÍA CORTES ASOCIACIÓN EN PARTICIPACIÓN ', '', ''),
('391-RAT', 'GRUPO FLORA ', 3, 'admon-gpoflora@hotmail.com', ' MARIA MAGDALENA RAMIREZ TAPIA  ', '', ''),
('392-FCS', 'SIMARTEC ', 3, 'jesse@simartec.com.mx', ' FABRICACIONES Y COMPONENTES INDUSTRIALES SIMARTEC S.A. DE C.V ', '', ''),
('394-ORX', 'PATIO REVOLUCION', 5, 'gteadmon@elipark.com', ' OPERADORA RICKY S.A DE C.V ', '', ''),
('395-OEA', 'ELIPARK ACAPULCO', 5, '', ' OPERADORA ELISA S.A DE C.V ', '', ''),
('396-OEV', 'ELIPARK CUERNAVACA', 5, '', ' OPERADORA ELISA S.A DE C.V ', '', ''),
('398-RTS', 'TWIST', 7, 'cpdelmoralr@hotmail.com', ' RESORTES TWIST DE MEXICO S DE RL DE CV ', '', ''),
('399-SAX', 'SERVIMEALS', 7, 'jtorres@servimeals.com', ' SERVICIOS ALIMENTARIOS SERVIMEALS S. DE R.L. DE C.V. ', '', ''),
('400', 'RYCKY KRYSTAL ', 5, '', '', '', ''),
('401', 'LAST MINUTE', 4, 'lastminute.viajes@gmail.com', ' LM PROTECCIONES AERONAUTICAS SA DE CV ', '', ''),
('402', 'GALEX', 7, 'gtorresm@live.com.mx', ' JUAN GABRIEL TORRES MONJARAS ', '', ''),
('403', 'CAFETERIA CRIT', 2, 'rentafiestacancun@hotmail.com', ' JOSÉ ALBERTO SOLOGUREN PEÑA  ', '', ''),
('407', 'MENDOZA BARBOSA', 3, 'mendozabarbosa@gmail.com', ' JOSSUE FERNANDO MENDOZA BARBOSA ', '', ''),
('411', 'EIGNER ', 8, 'alejandronarro@asc.com.mx', ' EIGNER SA DE CV ', '', ''),
('412', 'SOLDAT ENPRESA || TARGAMEX', 6, 'lfranco@globalsolutionsmexico.com.mx', ' SOLDAT ENPRESA S.A. DE C.V. ', '', ''),
('413', 'ELIPARK AUDITORES', 5, '', ' OPERADORA ELISA S.A DE C.V ', '', ''),
('414', 'ELIPARK PTO VALLARTA', 5, '', ' OPERADORA ELISA S.A DE C.V ', '', ''),
('415', 'ELIPARK CANCUN ', 5, '', ' OPERADORA ELISA S.A DE C.V ', '', ''),
('416', 'SOLUCIONES ESTRATEGICAS ORG', 7, 'ltobias@prodigy.net.mx', ' SOLUCIONES ESTRATEGICAS ORGANIZACIONALES S.C. ', '', ''),
('417', 'SOLUCIONES ESTRATEGICAS ORG', 7, '', ' SOLUCIONES ESTRATEGICAS ORGANIZACIONALES S.C. ', '', ''),
('418', 'MARCHARBEL MUÑOZ', 8, 'dmadrigalmedina@gmail.com', ' MAR CHARBEL HOSPITAL SC  ', '', ''),
('419', 'AVANTE BONFIL', 6, 'karlap80@hotmail.com', ' KARLA PAOLA AVILES ROMERO  ', '', ''),
('423-FGE', 'GENTE EXITOSA', 3, 'training.ochoa@gmail.com', ' FORMACION DE GENTE EXITOSA SC ', '', ''),
('425-MCT', 'TRIPNOW', 4, 'contabilidad@cancunenoferta.com', ' MEXICAN CARIBE TRAVELS S.A DE C.V ', '', ''),
('428-SAL', 'SEVISIBLE', 8, 'alejandra@sevisible.com.mx', ' ADRIANA SAUCEDO LOPEZ ', '', ''),
('429-GAB', 'GWENDOLYNE ', 4, 'gwendolynegama@yahoo.com.mx', ' GWENDOLYNE ELENA GAMA BENITEZ ', '', ''),
('431-EAX', 'EQUIPAR', 6, 'administracion@revistaequipar.com', ' EDITORAR ASOCIADOS SA DE CV ', '', ''),
('432-APR', 'APRENDERE PRESS', 8, 'jsmontante@gmail.com', ' APRENDERE PRESS S.A. DE C.V. ', '', ''),
('433-TOX', 'PROCESAR', 7, 'ctorres@procesarslp.com', ' CESAR TORRES ARAUJO  ', '', ''),
('435-NRS', 'SENSEBENE', 7, 'andrea_loredo@hotmail.com', ' NEGOCIOS RENTABLES DE SAN LUIS S.A. DE C.V. ', '', ''),
('437-FAB', 'FILTER BATTERY', 7, 'nominas-medina@hotmail.com', ' FILTER & BATTERY EXPERTS SA DE CV  ', '', ''),
('438-GCA', 'GOURMEATS RESTAURANTE MARE', 5, '', ' GOURMEATS CANCUN SA DE CV ', '', ''),
('439-SIX', 'INGLES INDIVIDUAL ', 8, 'adiliasm@gmail.com', ' ADILIA SIQUEIROS MONDACA ', '', ''),
('440-SIX', 'INGLES COMISIONISTA SEMANAL', 8, '', ' ADILIA SIQUEIROS MONDACA ', '', ''),
('442-SSL', 'SANITARIOS SLP', 8, '', ' SANITARIOS DE SAN LUIS S DE RL DE CV  ', '', ''),
('443-VIF', 'LUCIO TZAB', 6, '', ' SIN FACT AL CLIENTE POR EL MOMENTO  ', '', ''),
('447-OET', 'ELIPARK TUXTLA', 5, '', ' OPERADORA ELISA S.A DE C.V ', '', ''),
('448-COE', 'SUPPORT CONSULTORES', 5, '', ' KAREN CORREA ESPINOLA  ', '', ''),
('449-MAG', 'MATE AGENCY', 3, '', ' MATE AGENCY SC  ', '', ''),
('450-APX', 'ROLANDIS', 2, '', ' ASOCIACION DE PERFILES SA DE CV  ', '', ''),
('451-IRI', 'COMEDOR URBANO', 2, '', ' INMOBILIARIA RIVERDI SA DE CV ', '', ''),
('453-ODE', 'OCEAN DENTAL SEMANAL', 4, '', ' OCEAN DENTAL S DE RL DE CV ', '', ''),
('454-CAH', 'CONSULTORES ASOCIADOS', 4, '', ' CONSULTORES ASOCIADOS HOLGUIN AÑOVEROS ', '', ''),
('455-ORP', 'ELIPARK PUERTO CANCUN ', 5, '', ' OPERADORA ELISA S.A DE C.V ', '', ''),
('461', 'ROYAL VACATIONS', 4, '', ' TRAVEL TRANSFERS DE CANCUN SA DE CV ', '', ''),
('462', 'TIGRES', 6, '', 'CBBTQROO SOCIEDAD ANONIMA PROMOTORA DE INVERSION DE CAPITAL VARIABLE', '', ''),
('463', 'ESCUELA CULINARIA', 7, '', ' ESCUELA CULINARIA DE SAN LUIS POTOSI SC ', '', ''),
('464', 'ANTONIO AZCORRA', 1, '', '', '', ''),
('465', 'PATIO TLALPAN', 5, '', ' OPERADORA RICKY SA DE CV ', '', ''),
('466', 'VGP CONSTRUCCIONES', 3, '', ' VGP CONSTRUCCIONES SA DE CV  ', '', ''),
('468', 'LOCER', 8, '', ' ADRIAN RICARDO LOPEZ CERVANTES ', '', ''),
('469', 'ELIPARK LEGARIA CDMX/OPERADORA RICKY CD DE MEXICO', 5, '', ' OPERADORA Y ADMINISTRADORA DE ESTACIONAMIENTOS  ELISA SA DE CV ', '', ''),
('470', 'SANINOX', 8, '', ' SANINOX SA DE CV ', '', ''),
('471', 'BENY FERRETERIAS', 8, '', ' BENITO MATA CAMPUZANO ', '', ''),
('472', 'ELIPARK SOLARE', 5, '', ' OPERADORA Y ADMINISTRADORA DE ESTACIONAMIENTOS  ELISA SA DE CV ', '', ''),
('474', 'COATZACOALCOS', 5, '', ' OPERADORA Y ADMINISTRADORA DE ESTACIONAMIENTOS  ELISA SA DE CV ', '', ''),
('476', 'WALMART INTERLOMAS', 5, '', ' OPERADORA Y ADMINISTRADORA DE ESTACIONAMIENTOS  ELISA SA DE CV ', '', ''),
('478-CUI', 'ELIPAK CUICUILCO', 5, '', ' OPERADORA ELISA SA DE CV ', '', ''),
('479-ENA', 'ELIPAK NAYARIT', 5, '', ' OPERADORA ELISA SA DE CV ', '', ''),
('480-EVC', 'ELIPAK CHALCO', 5, '', ' OPERADORA Y ADMINISTRADORA DE ESTACIONAMIENTOS ELISA SA DE CV ', '', ''),
('481', 'INGLES INDIVIDUAL CD JUAREZ', 8, '', ' PAULINA GONZALEZ PRIETO   ', '', ''),
('485', 'TRIBU', 5, '', ' GOURMEATS CANCÚN S.A. DE C.V. ', '', ''),
('486', 'LIRIOS', 6, '', '', '', ''),
('489-VMC', 'VILLAS MAGNA', 2, '', ' VILLA MAGNA CANCUN MANAGEMENT A.C.  ', '', ''),
('490', 'WAYAN NATURAL', 3, 'fcime@wayan.com.mx', ' WAYAN NATURAL WEAR SA DE CV ', '', ''),
('493', 'SIECA  QUICENAL', 7, '', 'SIECA SOLUCIONES  Y SERVICIOS S D RL DE CV', '', ''),
('494', 'EQUINOX', 4, '', 'EQUINOX INDUSTRIAS SA DE CV', '', ''),
('496', 'IMPRENTA QUINCENAL', 3, '', 'IMPRENTA Y PUBLICIDAD DEL SURESTE S.A DE C.V', '', ''),
('497', 'IMPRENTA SEMANAL', 3, '', 'IMPRENTA Y PUBLICIDAD DEL SURESTE S.A DE C.V', '', ''),
('498', 'GRUPO EL TORO ', 3, '', 'JOEL AVALOS LARA', '', ''),
('500', '500 RH & R UNIVERSAL', 5, '', 'RH&R UNIVERSAL S.C', '', ''),
('501', 'HERVIAL MERIDA', 6, '', '', '', ''),
('502', 'HERVIAL CAMPECHE', 6, '', '', '', ''),
('503', 'COMLUXMEX', 6, '', '', '', ''),
('70', 'MPC', 6, '', '', '', ''),
('88', 'JUAN MANUEL ROCHA', 3, '', '', '', ''),
('CAT-483', 'KAPUNI ', 3, '', ' ERICK ORLANDO CASTILLA TRISTAN ', '', ''),
('CRE-504', 'LYCHEE', 6, '', 'CARA DE RES S. DE R.L. DE C.V.', '', ''),
('GOP-505', 'INGLES INDIVIDUAL CHIHUAHUA', 8, '', '', '', ''),
('KGR-488', 'HERVIAL', 6, '', ' KILIZA GROUP SA DE CV ', '', ''),
('NUB-022', 'Antonios Pizza', 1, 'aazcorra@azcorra.com', 'Pizzas del caribe sa de cv', 'Antonio Azcorra', 'http://localhost:8888/owen/index.php?id=NUB-022'),
('PAU-001', 'Omartz Inc', 1, 'omar.paulino@me.com', 'Omar SA de CV', 'Omar Paulino ', 'http://localhost:8888/owen/index.php?id=PAU-001'),
('SEM-484', 'SEMALTEC ', 3, '', ' SEMALTEC S.A. DE C.V. ', '', ''),
('SIC-492', 'LALUDO (LUDOTECAS)', 7, '', 'ANA LUCIA SILVA CANTU', '', ''),
('SIX-491', 'INGLES INDIVIDUAL CD JUAREZ COM.', 8, '', 'PAULINA GONZALEZ PRIETO  ', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `departamentos`
--

CREATE TABLE `departamentos` (
  `id_departamento` int(2) NOT NULL,
  `departamento` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departamentos`
--

INSERT INTO `departamentos` (`id_departamento`, `departamento`) VALUES
(4, 'Contabilidad'),
(2, 'Enlace'),
(5, 'Juridico'),
(6, 'Marketing'),
(3, 'Operaciones'),
(1, 'Tesoreria');

-- --------------------------------------------------------

--
-- Table structure for table `ejecutivos`
--

CREATE TABLE `ejecutivos` (
  `id_ejecutivo` int(4) NOT NULL,
  `ejecutivo` char(30) NOT NULL,
  `estatus` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ejecutivos`
--

INSERT INTO `ejecutivos` (`id_ejecutivo`, `ejecutivo`, `estatus`) VALUES
(1, 'Antonio Azcorra', 'Activo'),
(2, 'Sarai Ortega', 'Activo'),
(3, 'Candy Gonzalez', 'Activo'),
(4, 'Claudia Reynaga', 'Activo'),
(5, 'Edgar Cohuo', 'Activo'),
(6, 'Oscar Basto', 'Activo'),
(7, 'Hector Puerta', 'Activo'),
(8, 'Delia Colunga', 'Activo'),
(9, 'Omar Paulino', 'Inactivo');

-- --------------------------------------------------------

--
-- Table structure for table `encuestas`
--

CREATE TABLE `encuestas` (
  `id` varchar(20) NOT NULL,
  `respuestas` varchar(200) NOT NULL,
  `id_empresa` varchar(50) NOT NULL,
  `id_ejecutivo` int(2) NOT NULL,
  `fecha` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `encuestas`
--

INSERT INTO `encuestas` (`id`, `respuestas`, `id_empresa`, `id_ejecutivo`, `fecha`) VALUES
('1', 'si', '027MAQ', 4, '2018-11-15'),
('2', 'si', '027MAQ', 4, '2018-11-15'),
('3', 'si', '027MAQ', 4, '2018-11-15'),
('4', 'si', '027MAQ', 4, '2018-11-15'),
('5', 'si', '027MAQ', 4, '2018-11-15'),
('6', 'si', '027MAQ', 4, '2018-11-15'),
('7', 'si', '027MAQ', 4, '2018-11-15'),
('1_1', '10', '027MAQ', 4, '2018-11-15'),
('1_2', '10', '027MAQ', 4, '2018-11-15'),
('comentarios', 'ANEUDY', '027MAQ', 4, '2018-11-15');

-- --------------------------------------------------------

--
-- Table structure for table `preguntas2`
--

CREATE TABLE `preguntas2` (
  `id_pregunta2` int(4) NOT NULL,
  `pregunta` varchar(200) NOT NULL,
  `departamento` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `preguntas2`
--

INSERT INTO `preguntas2` (`id_pregunta2`, `pregunta`, `departamento`) VALUES
(1, '¿La atención que le dimos durante este periodo de nómina fue el adecuado?', 'Enlace'),
(2, '¿Recibió su prenómina a tiempo?', 'Enlace'),
(3, '¿Su dispersíon fue correcta?', 'Tesoreria'),
(4, '¿Recibió puntualmente sus recibos de nómina?', 'Nominas'),
(5, '¿Ha recibido soluciones oportunas por parte del dpto. De Jurídico?', 'Juridico'),
(6, '¿Le llegaron a tiempo sus facturas?', 'Nominas'),
(7, 'Vive en Cancun?', 'Enlace');

-- --------------------------------------------------------

--
-- Table structure for table `preguntas10`
--

CREATE TABLE `preguntas10` (
  `id_pregunta10` int(2) NOT NULL,
  `id_secundario` decimal(2,1) NOT NULL,
  `pregunta` varchar(100) NOT NULL,
  `departamento` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `preguntas10`
--

INSERT INTO `preguntas10` (`id_pregunta10`, `id_secundario`, `pregunta`, `departamento`) VALUES
(1, '1.1', '¿Como califica el servicio de su ejecutivo en cuenta?', 'Enlace'),
(2, '1.2', '¿Como califica el servicio de Owen Group?', 'Enlace');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(2) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `clave` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre`, `clave`) VALUES
(1, 'Omar Paulino', 'Mabel1q1'),
(2, 'admin', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD UNIQUE KEY `id_cliente` (`id_cliente`);

--
-- Indexes for table `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id_departamento`),
  ADD UNIQUE KEY `departamento` (`departamento`);

--
-- Indexes for table `ejecutivos`
--
ALTER TABLE `ejecutivos`
  ADD PRIMARY KEY (`id_ejecutivo`);

--
-- Indexes for table `preguntas2`
--
ALTER TABLE `preguntas2`
  ADD PRIMARY KEY (`id_pregunta2`);

--
-- Indexes for table `preguntas10`
--
ALTER TABLE `preguntas10`
  ADD PRIMARY KEY (`id_pregunta10`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id_departamento` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ejecutivos`
--
ALTER TABLE `ejecutivos`
  MODIFY `id_ejecutivo` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `preguntas2`
--
ALTER TABLE `preguntas2`
  MODIFY `id_pregunta2` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `preguntas10`
--
ALTER TABLE `preguntas10`
  MODIFY `id_pregunta10` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
