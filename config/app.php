<?php
session_start();
define('APP_PATH', realpath(__DIR__.'/../'));
date_default_timezone_set('America/Cancun');
// Helper
// TODO: Refactor
function url($path = '')
{
    $url = getenv('APP_URL');
    $url = rtrim($url, '/');
    $url .= $path;
    return $url;
}

function slugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

// Require vendor files
require_once APP_PATH.'/vendor/autoload.php';
require_once APP_PATH.'/app/Routes/routes.php';

// Load env conf
$dotenv = Dotenv\Dotenv::create(APP_PATH.'/');
$dotenv->load();

// Load url
$route = (isset($_GET['url']) && !empty($_GET['url'])) ? App\Libraries\Core\Route::routing($_GET['url']) : false;
