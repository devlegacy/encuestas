<?php
include(__DIR__.'/config/app.php');

if ($route) :
else :
  include('consultas.php');
?>
  <!DOCTYPE html>
  <html lang="es">
  <head>
      <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user_scalable=no, initial-scale=1.0, maximum-scale=1.0, minimium-scale=1.0">
      <title>Encuesta de satisfacción</title>
      <link rel="stylesheet" type="text/css" href="estilos/estilo.css">
  </head>

  <?php

  // Ejecutar comprobar si existe algun error
  if (!$resultado = $mysqli->query($sqlclientesindex)) {
      echo "Error: La ejecución de la consulta falló debido a: \n";
      echo "Query: " . $sql . "\n";
      echo "Errno: " . $mysqli->errno . "\n";
      echo "Error: " . $mysqli->error . "\n";
      exit;
  }
  //obtenemos el resultado

  if ($resultado->num_rows > 0) {
      while ($cliente = $resultado->fetch_assoc()) {
          $cliente2 = $cliente['cliente'];
          $idcliente2 = $cliente['id_cliente'];
          $idejecutivo2 = $cliente['id_ejecutivo'];
          $idcontacto= $cliente['id_contacto'];
      }
  } else {
      echo 'No se encontraron resultados';
      exit;
  }

  $sqlejecutivo = "SELECT * FROM ejecutivos where id_ejecutivo = $idejecutivo2";

  if (!$ejecutivos = $mysqli->query($sqlejecutivo)) {
      echo "Error: La ejecución de la consulta falló debido a: \n";
      echo "Query: " . $sqlejecutivo . "\n";
      echo "Errno: " . $mysqli->errno . "\n";
      echo "Error: " . $mysqli->error . "\n";
      exit;
  }

  if ($ejecutivos->num_rows > 0) {
      while ($ejecutivo = $ejecutivos->fetch_assoc()) {
          $idejecutivoo = $ejecutivo['ejecutivo'];
          $idejecitivonumber= $ejecutivo['id_ejecutivo'];
      }
  } else {
      echo 'No se encontraron resultados';
      exit;
  }

  //consulta Tabla de Antiguedad Owen
  $sqlantiguedad = "SELECT * FROM antiguedad";

  if (!$antiguedad = $mysqli->query($sqlantiguedad)) {
      echo "Error: La ejecución de la consulta falló debido a: \n";
      echo "Query: " . $sqlejecutivo . "\n";
      echo "Errno: " . $mysqli->errno . "\n";
      echo "Error: " . $mysqli->error . "\n";
      exit;
  }

  // Consulta Como se entero de owen

  $sqlentero = "SELECT * FROM entero";

  if (!$entero = $mysqli->query($sqlentero)) {
      echo "Error: La ejecución de la consulta falló debido a: \n";
      echo "Query: " . $sqlejecutivo . "\n";
      echo "Errno: " . $mysqli->errno . "\n";
      echo "Error: " . $mysqli->error . "\n";
      exit;
  }

  $for=$mysqli->query($sql2);
  $for10=$mysqli->query($sql3);

  ?>

  <body>

      <form action="conexion.php" method="post" class="form-register">
      <h2 class="form_titulo">Encuesta de satisfacción Owen Group</h2>
          <div class="contenedor-inputs">
              <p>  Estimado(a) <b><i><?php echo $idcontacto; ?></i></b> , en Owen Group queremos brindarle siempre un excelente servicio.
              <br> Ayúdenos a mejorar contestando esta breve encuesta de calidad: </p></p>
                          <input type="hidden" name="cliente1" value=<?php echo $idcliente2 ?>></input>
                          <input type="hidden" name="ejecutivobd" value=<?php echo $idejecitivonumber ?>></input>
  <div class="div_radio1">
        <p class="p_escala1-10"> ¿Cómo conoció a Owen Group?</p>
          <select name = "conocio" class="select_dpto">
      <?php foreach ($entero as $ent) : ?>
              <p><option name = "entero" value=<?php echo $ent[id_entero] ?>><?php echo $ent['entero']; ?></option></p>

      <?php endforeach; ?>
              </select>
      <p class="p_escala1-10"> ¿Cuánto tiempo lleva utilizando nuestros servicios?</p>
          <select name = "tiempo" class="select_dpto">
      <?php foreach ($antiguedad as $ant) : ?>
              <p><option name = "antiguedad" value=<?php echo $ant[id_antiguedad] ?>><?php echo $ant['antiguedad']; ?></option></p>

      <?php endforeach; ?>
              </select>
  </div>
  <div class="div_radio1">
      <p class="p_escala1-10"> Indique por favor cómo califica el servicio de Owen Group en general:</p>
            <p><label class="radioContainer">Muy malo<input type="radio" name="pregunta1" value="1" required>
                  <span class="circle"></span>
            </label>
              <label class="radioContainer">Malo<input type="radio" name="pregunta1" value="2" required>
                  <span class="circle"></span></label>
              <label class="radioContainer">Regular<input type="radio" name="pregunta1" value="3" required><span class="circle"></span></label>
              <label class="radioContainer">Bueno<input type="radio" name="pregunta1" value="4" required>
                  <span class="circle"></span></label>
              <label class="radioContainer">Excelente<input type="radio" name="pregunta1" value="5" required><span class="circle"></span></label>
          </p>
  </div>
  <div class="div_radio1">
      <p class="p_escala1-10"> Qué calificación le daría a Owen Group, en:</p>
      <?php foreach ($for as $option) : ?>
              <p><?php echo $option[ID] ?><?php echo ' '?><?php echo $option[pregunta] ?>
              <br><label class="radioContainer">Muy malo<input type="radio" name=<?php echo $option[id_pregunta2] ?> value="1" required>
                  <span class="circle"></span>
              </label>
              <label class="radioContainer">Malo<input type="radio" name=<?php echo $option[id_pregunta2] ?> value="2" required>
                  <span class="circle"></span>
              </label>
              <label class="radioContainer">Regular<input type="radio" name=<?php echo $option[id_pregunta2] ?> value="3" required>
                  <span class="circle"></span>
              </label>
              <label class="radioContainer">Bueno<input type="radio" name=<?php echo $option[id_pregunta2] ?> value="4" required>
                  <span class="circle"></span>
              </label>
              <label class="radioContainer">Excelente<input type="radio" name=<?php echo $option[id_pregunta2] ?> value="5" required>
              <span class="circle"></span>
          </label>
          </p>
      <?php endforeach; ?>
  </div>
  <div class="div_radio1">
      <p class="p_escala1-10"> Cómo calificaría los conocimientos de su ejecutivo de cuenta, en los siguientes temas:</p>

      <?php foreach ($for10 as $option10) : ?>
              <p><?php echo $option10[ID10] ?><?php echo ' '?><?php echo $option10[pregunta] ?>
                        <br><label class="radioContainer">Muy malo<input type="radio" name=<?php echo $option10[id_secundario] ?> value="1" required>
                        <span class="circle"></span>
                    </label>
                              <label class="radioContainer">Malo<input type="radio" name=<?php echo $option10[id_secundario] ?> value="2" required>
                              <span class="circle"></span>
                          </label>
                              <label class="radioContainer">Regular<input type="radio" name=<?php echo $option10[id_secundario] ?> value="3" required>
                              <span class="circle"></span>
                          </label>
                              <label class="radioContainer">Bueno<input type="radio" name=<?php echo $option10[id_secundario] ?> value="4" required>
                              <span class="circle"></span>
                          </label>
                              <label class="radioContainer">Excelente<input type="radio" name=<?php echo $option10[id_secundario] ?> value="5" required>
                              <span class="circle"></span>
                          </label>
              </p>
      <?php endforeach; ?>
  </div>
  <div class="div_radio1">
      <p class="p_escala1-10"> ¿Recibe el apoyo necesario cuando se le presenta un problema y/o duda? </p>
            <p><label class="radioContainer">Sí<input type="radio" name="apoyo" value="si" required>
                  <span class="circle"></span>
            </label>
              <label class="radioContainer">No<input type="radio" name="apoyo" value="no" required>
                  <span class="circle"></span></label>
                  </p>
              </div>
  <div class="div_radio1">
                  <P class="p_escala1-10">Si usted NO ha recibido la asesoría adecuada, por favor coméntenos sobre su experiencia, para rectificar nuestra atención y perfeccionarla para usted:</P>
                  <textarea rows="4" cols="100%" name="comentarios" class="contenedor-texto"></textarea>

                  <P class="p_escala1-10">¿Qué le gusta de nuestro servicio?</P>
                  <textarea rows="4" cols="100%" name="comentarios1" class="contenedor-texto"></textarea>

                  <P class="p_escala1-10">¿Qué NO le gusta de nuestro servicio?</P>
                  <textarea rows="4" cols="100%" name="comentarios2" class="contenedor-texto" id="contenedor_ultimo"></textarea>
  </div>
  <div class="div_radio1">
      <p class="p_escala1-10"> ¿Recomendaría nuestros servicios a otras empresas?</p>
          <p>
              <label class="radioContainer">Definitivamente<input type="radio" name="recomendacion" value="Definitivamente" required><span class="circle"></span></label>
              <label class="radioContainer">Sí<input type="radio" name="recomendacion" value="si" required><span class="circle"></span></label>
              <label class="radioContainer">Probablemente<input type="radio" name="recomendacion" value="Probablemente" required><span class="circle"></span></label>
              <label class="radioContainer">No<input type="radio" name="recomendacion" value="No" required><span class="circle"></span></label>
          </p>
  </div>
                  <p class="p_texto">¡Muchas gracias por su tiempo y opinión! Queremos mejorar para usted.</p>


                  <input type="submit" value="Enviar datos" class="input_enviar">

  </form>
  </body>
  </html>
<?php
endif
?>
