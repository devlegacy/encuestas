/**
 *
 * Generador de graficas
 */
(function (d) {

  const canvasTemplate = (id) => `<canvas id="${id}" width="" height=""></canvas>`;
  const commentParagraphTemplate = (comments) => {
    let paragraphHTML = '';
    comments.forEach(comment => paragraphHTML += `<p class="divider">${comment}</p>`);
    return paragraphHTML;
  };
  const commentTemplate = (comment) => (`
    <article class="card w90">
      <h4 class="card__header">${comment.title}</h4>
      <div class="card__body">${commentParagraphTemplate(comment.comments)}</div>
    </article>
  `);
  const executiveTemplate = (executive) => (`
    <div class="profile">
      <div class="profile__picture"></div>
      <p class="profile__name">${executive.ejecutivo}</p>
    </div>
  `);
  function prepareCanvas(response) {
    // handle success
    const charts = response.data.charts || [];
    graphsContainer.innerHTML = '<h3>Gráficas:</h3>';
    commentsContainer.innerHTML = '<h3>Comentarios:</h3>';
    executiveContainer.innerHTML = '<h3>Ejecutivos</h3>';
    let template = '';
    charts.forEach(chart => {
      if (chart.questions.length) {
        template += `<fieldset> ${chart.title ? `<legend>${chart.title}</legend>` : ''}`;
        chart.questions.forEach(c => {
          template += canvasTemplate(c.canvas);
        });
        template += `</fieldset>`;
      }
    });
    graphsContainer.insertAdjacentHTML('beforeend', template);
    return response;
  }
  function generateGraphs(response) {
    const charts = response.data.charts;
    const comments = response.data.comments;
    const executives = response.data.executives || [];

    charts.forEach(chart => {
      chart.questions.forEach(c => {
        const ctx = document.getElementById(c.canvas).getContext('2d');
        new Chart(ctx, {
          type: 'pie',
          data: {
            labels: c.labels,
            datasets: [{
              backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#1D3461"],
              data: c.data
            }]
          },
          tooltips: {
            enabled: false,
            mode: 'index',
            position: 'nearest',
            custom: function (tooltip) {
              console.log(tooltip);
            }
          },
          options: {
            title: {
              display: true,
              text: c.title,
            },
            onAnimationComplete: function () {
              this.showTooltip(this.segments, true);
            },
            showTooltips: true,
          }
        })
      });
    });

    comments.forEach(comment => {
      commentsContainer.insertAdjacentHTML('beforeend', commentTemplate(comment));
    });

    executives.forEach(executive => {
      executiveContainer.insertAdjacentHTML('beforeend', executiveTemplate(executive));
    });

  }

  const graphsContainer = d.getElementById('graphs-container');
  const commentsContainer = d.getElementById('comments-container');
  const executiveContainer = d.getElementById('executive-container');
  const frmGraphsByCompany = d.getElementById('frm-graphs-by-company');
  const frmGraphsByCompanyAndMonth = d.getElementById('frm-graphs-by-company-and-month');
  if (graphsContainer &&
    commentsContainer &&
    executiveContainer &&
    frmGraphsByCompany &&
    frmGraphsByCompanyAndMonth) {

    frmGraphsByCompany.addEventListener('submit', (e) => {
      e.target.querySelector('.btn-bouncing-loader > div').classList.add('bouncing-loader');
      e.preventDefault();
      const questions = Array.from(e.target.questions).filter(question => question.checked).map(question => question.value);
      const data = {
        from: e.target.from.value,
        to: e.target.to.value,
        company: e.target.company.value,
        questions,
        totalCuestions: e.target['total-questions'].value,
      };
      axios.post('/?url=xhr/estadisticas/general', data)
        .then(prepareCanvas)
        .then(generateGraphs)
        .then(() => {
          console.log('last');
          e.target.querySelector('.btn-bouncing-loader > div').classList.remove('bouncing-loader');
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        });
    });

    frmGraphsByCompanyAndMonth.addEventListener('submit', (e) => {
      e.target.querySelector('.btn-bouncing-loader > div').classList.add('bouncing-loader');
      e.preventDefault();
      const questions = Array.from(e.target.questions).filter(question => question.checked).map(question => question.value);
      const data = {
        month: e.target.month.value,
        questions,
        totalCuestions: e.target['total-questions'].value,
      };
      axios.post('/?url=xhr/estadisticas/mensual', data)
        .then(prepareCanvas)
        .then(generateGraphs)
        .then(() => {
          console.log('last');
          e.target.querySelector('.btn-bouncing-loader > div').classList.remove('bouncing-loader');
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        });
    });
  }

})(document);
