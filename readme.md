# Gráficas

## Install composer

[Composer](https://getcomposer.org/download/)

## Excecute in project folder

```bash
  composer install
```

## Create file .env (enviroment)

```bash
  touch .env
```

## Copy the .env.example content to .env

## Create quiz results

Go to: http://[server-name]/?id=0022

[Example](http://encuestas.localhost/?id=0022)

## Go to:

http://[server-name]/?url=estadisticas/general

[Example](http://encuestas.localhost/?url=estadisticas/general)

## Logs de error

sudo tail /var/log/nginx/error.log

