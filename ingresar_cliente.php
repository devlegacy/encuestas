<?php
  error_reporting(0); //Quitar todas las Alertas

	session_start();
	$varsesion = $_SESSION['usuario'];
	if($varsesion == null || $varsesion == ''){
		header('Location: mensajes/autorizacion.php');
		die();
	}
?>
<?php
include('consultas.php')
?>
<?php

// Realizar una consulta SQL
$sqlejecutivos = "SELECT * FROM ejecutivos";

// Ejecutar comprobar si existe algun error
if (!$departamento_ejecutivo = $mysqli->query($sqlejecutivos)) {
    echo "Error: La ejecución de la consulta falló debido a: \n";
    echo "Query: " . $sqlejecutivos . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Agregar al cliente nuevo</title>
	<link rel="stylesheet" type="text/css" href="estilos/estilo_cliente.css">
  <script type='text/javascript'>
function confirmDelete()
{
   return confirm("Estas seguro de Eliminar este Registro?");
}
</script>
  <script type='text/javascript'>
function confirmSend()
{
   return confirm("Estas seguro de enviar las Encuestas?");
}
</script>
</head>
<body>
	<div class="div_header">
		<header>
			<h1>Bienvenido <?php echo $_SESSION['usuario'] ?></h1>
			<a href="cerrar_sesion.php" class="cerrar_sesion">Cerrar Sesión</a>
		</header>
	</div>
	<form action="conexion_cliente.php" method="post" class="form-register">
	<h2 class="form_titulo">Agregar Cliente Nuevo</h2>
		<div class="contenedor-inputs">
  			    <input type="text" name="id" class="input_id" placeholder="ID Cliente" required>
            <input type="text" name="cliente" class="input_cliente" placeholder="Nombre Comercial" required>
            <input type="text" name="razon_social" class="input_razon" placeholder="Razon Social" required>
            <input type="text" name="contacto" class="input_contacto" placeholder="Contacto" required>
            <input type="email" name="email" class="input_email" placeholder="Correo Electronico" required>
  			<select name = "ejecutivo" class="select_ejecutivo">

     <?php foreach ( $departamento_ejecutivo as $option ) : ?>
            <p><option name = "ejecutivo" value=<?php echo $option[id_ejecutivo] ?>><?php echo $option['ejecutivo']; ?></option></p>           

     <?php endforeach; ?>
  			</select>
		    	<input type="submit" value="Registrar Cliente" id="input_boton" class="enviar">
		    	<a href="panel.php" value="Cancelar" id="input_boton" class="cancelar">Regresar</a>
</form> 

<h3 id="filtrar">Filtrar por:</h3> 
  <form id="buscador" name="buscador" method="post" action="busquedacliente.php"> 
      <select name = "ejecutivo" class="select2">
            <?php foreach ( $departamento_ejecutivo as $option ) : ?>
              <p><option name = "ejecutivo" value=<?php echo $option[id_ejecutivo] ?>><?php echo $option['ejecutivo']; ?></option></p>           
            <?php endforeach; ?>  
        </select>
              <input type="submit" name="buscador" value="Buscar" id="btn_buscar"></input>
  </form>
            <h3 id="send_todos">Enviar Correo a todos:</h3>
            <form action="sendmailall.php" method="post">  
                        <input type="submit" value="Enviar" id="btn_buscar" onclick='return confirmSend()'></input>
            </form>
          
  <table>
    <thead>
      <tr>
        <th class="tr">ID</th>
        <th class="tr">Cliente</th>
        <th class="tr">Ejecutivo</th>
        <th class="tr">Contacto</th>
        <th class="tr">Correo</th>
        <th class="tr">Link</th>
        <th class="action">Accion</th>
      </tr>
    </thead>

<?php
// Realizar una consulta SQL
$sql6 = "SELECT * FROM clientes";

// Ejecutar comprobar si existe algun error
if (!$cliente = $mysqli->query($sql6)) {
    echo "Error: La ejecución de la consulta falló debido a: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}

?>
    <tbody>
     <?php foreach ( $cliente as $cli ) :
      ?>
      <tr>
        <td><?php echo $cli[id_cliente] ?></td>
        <td><?php echo $cli[cliente] ?></td>
<?php
 
$sqlejecutivo = "SELECT ejecutivo FROM ejecutivos where id_ejecutivo = $cli[id_ejecutivo]";

if (!$ejecutivos = $mysqli->query($sqlejecutivo)) {
    echo "Error: La ejecución de la consulta falló debido a: \n";
    echo "Query: " . $sqlejecutivo . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}

if ($ejecutivos->num_rows > 0){
    while ($ejecutivo = $ejecutivos->fetch_assoc()) {
           $idejecutivoo = $ejecutivo['ejecutivo'];
        }
}
else{
    exit;
}
?>
        <td><?php echo $idejecutivoo ?></td>
        <td><?php echo $cli[id_contacto] ?></td>
        <td><?php echo $cli[correo] ?></td>
        <td><a href=index.php?id<?php echo '='.$cli[id_cliente] ?>>Link</td>
        <td>
            <form action="sendmail.php" method="post">
                        <input type="hidden" name="email" value="<?php echo $cli[correo] ?>"></input>
                        <input type="hidden" name="link" value="<?php echo $cli[id_cliente] ?>"></input>
                        <input type="hidden" name="contacto" value="<?php echo $cli[id_contacto] ?>"\></input>    
                        <input type="submit" value="Send" id="input_boton2" class="send"></input>
            </form>
            <form method="POST" action="delete_cliente.php">
                        <input type="hidden" name="link" value=<?php echo $cli[id_cliente] ?>></input>   
                        <input type="submit" value="Delete" id="input_boton2" class="delete" onclick='return confirmDelete()'></input>
            </form>   
        </td>
      </tr>

     <?php endforeach; ?> 

    </tbody>
  </table>
            
</body>
</html>
<?php
// Cerrar la conexión
$mysqli->close();
?>

