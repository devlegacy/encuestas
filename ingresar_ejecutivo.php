<?php
	error_reporting(0); //Quitar las alertas

	session_start();
	$varsesion = $_SESSION['usuario'];
	if($varsesion == null || $varsesion == ''){
		header('Location: mensajes/autorizacion.php');
		die();
	}
?>
<?php
include('consultas.php')
?>
<?php

$ejecutivos=$mysqli->query($sql4);
error_reporting(0);
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Agregar ejecutivo</title>
	<link rel="stylesheet" type="text/css" href="estilos/estilo_ejecutivo.css">
</head>
<body>
	<div class="div_header">
		<header>
			<h1>Bienvenido <?php echo $_SESSION['usuario'] ?></h1>
			<a href="cerrar_sesion.php" class="cerrar_sesion">Cerrar Sesión</a>
		</header>
	</div>
	<form action="conexion_ejecutivo.php" method="post" class="form-register">
	<h2 class="form_titulo">Agregar Ejecutivo</h2>
		<div class="contenedor-inputs">
  			<input type="text" name="ejecutivo" class="input_ejecutivo" placeholder="Nombre del Ejecutivo" required pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}" title="No se permite usar numeros o caracteres especiales (@$#%, etc.)">
  			  			<select name="estatus" class="select_estatus">
  							<option name = "estatus" value="Activo">Activo</option>
  							<option name = "estatus" value="Inactivo">Inactivo</option>
  						</select>	
		    <input type="submit" value="Registrar Ejecutivo" id="input_boton" class="enviar">
		    <input type="submit" value="Cancelar" id="input_boton" onClick="history.back()" class="cancelar">
		 
	</form>
	<table>
		<thead>
			<tr>
				<th >ID</th>
				<th >Ejecutivo</th>
				<th >Estatus</th>
			</tr>
		</thead>
		<tbody>
     <?php foreach ( $ejecutivos as $ejecu ) : ?>
			<tr>
				<td><?php echo $ejecu[id_ejecutivo] ?></td>
				<td><?php echo $ejecu[ejecutivo] ?></td>
				<td><?php echo $ejecu[estatus] ?></td>
			</tr>
     <?php endforeach; ?>	
		</tbody>
	</table>
</body
</html>