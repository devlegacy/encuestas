<?php
error_reporting(0); //Quitar las alertas

	session_start();
	$varsesion = $_SESSION['usuario'];
	if($varsesion == null || $varsesion == ''){
		header('Location: mensajes/autorizacion.php');
		die();
	}
?>
<?php
include('consultas.php')
?>
<?php
$id_pegunta_10 = $_POST['pregunta10'];
?>
<?php

// Realizar una consulta SQL
$sqlpregunta10 = "SELECT * FROM departamentos";

// Ejecutar comprobar si existe algun error
if (!$departamento_pregunta10 = $mysqli->query($sqlpregunta10)) {
    echo "Error: La ejecución de la consulta falló debido a: \n";
    echo "Query: " . $sqlpregunta . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}

?>
 <?php
// Realizar una consulta SQL
$sql6 = "SELECT * FROM preguntas10 where id_pregunta10 = '$id_pegunta_10'";

// Ejecutar comprobar si existe algun error
if (!$preguntas = $mysqli->query($sql6)) {
    echo "Error: La ejecución de la consulta falló debido a: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Agrega la pregunta deseada</title>
	<link rel="stylesheet" type="text/css" href="estilos/estilo_preguntas.css">
</head>
<body>
	<div class="div_header">
		<header>
			<h1>Bienvenido <?php echo $_SESSION['usuario'] ?></h1>
			<a href="cerrar_sesion.php" class="cerrar_sesion">Cerrar Sesión</a>
		</header>
	</div>
	<form action="update_preguntas10.php" method="post" class="form-register">
	<h2 class="form_titulo">Modificar Pregunta</h2>
		<div class="contenedor-inputs">
			     <?php foreach ( $preguntas as $preg10 ) : ?>
  			<input type="text" name="pregunta" class="input_preguntas" value='<?php echo $preg10[pregunta] ?>' required>
  				<?php endforeach; ?> 
  			<select name = "depto" class="select_dpto">
     <?php foreach ( $departamento_pregunta10 as $option ) : ?>
            <p><option name = "depto" value=<?php echo $option[departamento] ?>><?php echo $option['departamento']; ?></option></p>           
     <?php endforeach; ?>	
       			<input type="hidden" name="pregunta10" value=<?php echo $id_pegunta_10 ?>></input>  
		    	<input type="submit" value="Modificar Pregunta" id="input_boton" class="enviar">
		    	<a href="ingresar_preguntas.php" value="Cancelar" id="input_boton" class="cancelar">Cancelar</a>
    </form>
    <table>
    <thead>
      <tr>
        <th >Ejecutivo</th>
        <th >Departamento</th>
      </tr>
    </thead>
    <tbody>
     <?php foreach ( $preguntas as $preg ) : ?>
      <tr>
        <td><?php echo $preg[pregunta] ?></td>
        <td><?php echo $preg[departamento] ?></td>
      </tr>
           <?php endforeach; ?> 

      </tbody>
  </table>
</body>
</html>
<?php

// Cerrar la conexión
$mysqli->close();
?>


