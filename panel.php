<?php
    session_start();
    $varsesion = $_SESSION['usuario'];
    if ($varsesion == null || $varsesion == '') {
        header('Location: mensajes/autorizacion.php');
        die();
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta charset="utf-8">
	<title>Panel Principal</title>
	<link rel="stylesheet" type="text/css" href="estilos/estilo_panel.css">
	<title></title>
</head>
<body>
	<div class="div_header">
		<header class="header">
			<h1>Bienvenido <?php echo $_SESSION['usuario'] ?></h1>
			<a href="cerrar_sesion.php" class="cerrar_sesion">Cerrar Sesión</a>
		</header>
	</div>
	<form action="#" method="post" class="form-register">
		<h2 class="form_titulo">Panel de administrador</h2>
		<div class="contenedor-inputs">
  			<a href="ingresar_ejecutivo.php" class="buton_panel">Nuevo Ejecutivo</a>
		   	<a href="ingresar_preguntas.php" class="buton_panel">Nueva Pregunta</a>
		   	<a href="ingresar_cliente.php" class="buton_panel">Nuevo Cliente</a>
		   	<a href="/?url=estadisticas/general" class="buton_panel">Estadisticas</a>
		</div>
	</form>


</body>
</html>
