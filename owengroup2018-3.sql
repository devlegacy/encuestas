-- phpMyAdmin SQL Dump
-- version 4.7.8
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 25-01-2019 a las 16:09:03
-- Versión del servidor: 5.5.59-MariaDB
-- Versión de PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `owengroup2018`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`owengroup2018`@`%` PROCEDURE `spBuscarPregunta10` ()  BEGIN

SET @row_number = 0;

select *,(@row_number:=@row_number + 1) AS ID
from preguntas10;

END$$

CREATE DEFINER=`owengroup2018`@`%` PROCEDURE `spBuscarPregunta2` ()  BEGIN

SET @row_number = 0;

select *,(@row_number:=@row_number + 1) AS ID
from preguntas2;

END$$

CREATE DEFINER=`owengroup`@`%` PROCEDURE `spInsertarPregunta10` (IN `pre` VARCHAR(255), IN `depto` VARCHAR(255))  BEGIN
	DECLARE  total DECIMAL(10,1);
	SELECT max(id_secundario) + 0.1 into total FROM owengroup2018.preguntas10;
    	
  insert into preguntas10(id_pregunta10,id_secundario, pregunta,departamento)
    values(null,total,pre,depto);
	/*						  
 select total as Total;    */

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `antiguedad`
--

CREATE TABLE `antiguedad` (
  `id_antiguedad` int(2) NOT NULL,
  `antiguedad` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `antiguedad`
--

INSERT INTO `antiguedad` (`id_antiguedad`, `antiguedad`) VALUES
(1, '0 - 6 meses'),
(2, '7 meses - 1 año'),
(3, '1 - 2 años'),
(4, '3 - 5 años'),
(5, '5 años o más');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` varchar(15) NOT NULL,
  `cliente` varchar(100) NOT NULL,
  `id_ejecutivo` int(2) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `razon_social` varchar(200) NOT NULL,
  `id_contacto` varchar(200) NOT NULL,
  `sucursal` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `cliente`, `id_ejecutivo`, `correo`, `razon_social`, `id_contacto`, `sucursal`) VALUES
('0022', 'Wifi Omartz Caribe', 1, 'omar.paulino@me.com', 'EL WIFI MAS POTENTE SA DE CV', 'Omar Paulino Juarez ', 'CANCUN'),
('0023', 'Taquerias Omartz', 2, 'sistemas@owengroup.mx', 'Taquerial el pobrano sa de cv', 'Omar Paulino ', 'CANCUN'),
('0025', 'Soportes Informaticos del Sureste Mexicano', 3, 'dbarrera@owengroup.mx', 'informatica SA de CV', 'David Barrera', 'CANCUN'),
('0026', 'Ricardos Restaursn', 8, 'rdiazr@owengroup.mx', 'El restauran feliz es de cv', 'Ricardo Díaz ', 'CANCUN'),
('0027', 'Redes del Caribe', 6, 'paujua33@hotmail.com', 'informatica segura sa de cv', 'Omar Pajo', 'CANCUN'),
('0028', 'Wifi Network Lan', 4, 'matus_231@hotmail.com', '2018 WIFI MAS POTENTE SA DE CV', 'Mabel Matus', 'CANCUN');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id_departamento` int(2) NOT NULL,
  `departamento` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id_departamento`, `departamento`) VALUES
(4, 'Contabilidad'),
(2, 'Enlace'),
(5, 'Juridico'),
(6, 'Marketing'),
(3, 'Operaciones'),
(1, 'Tesoreria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ejecutivos`
--

CREATE TABLE `ejecutivos` (
  `id_ejecutivo` int(4) NOT NULL,
  `ejecutivo` char(30) NOT NULL,
  `estatus` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ejecutivos`
--

INSERT INTO `ejecutivos` (`id_ejecutivo`, `ejecutivo`, `estatus`) VALUES
(1, 'Antonio Azcorra', 'Activo'),
(2, 'Sarai Ortega', 'Activo'),
(3, 'Candy Gonzalez', 'Activo'),
(4, 'Claudia Reynaga', 'Activo'),
(5, 'Edgar Cohuo', 'Activo'),
(6, 'Oscar Basto', 'Activo'),
(7, 'Hector Puerta', 'Activo'),
(8, 'Delia Colunga', 'Activo'),
(9, 'Ulises', 'Activo'),
(10, 'Sin Ejecutivo', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encuestas`
--

CREATE TABLE `encuestas` (
  `id` varchar(20) NOT NULL,
  `respuestas` varchar(200) NOT NULL,
  `id_empresa` varchar(50) NOT NULL,
  `id_ejecutivo` int(2) NOT NULL,
  `fecha` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `encuestas`
--

INSERT INTO `encuestas` (`id`, `respuestas`, `id_empresa`, `id_ejecutivo`, `fecha`) VALUES
('tiempo', '1', '0022', 1, '2018-12-03'),
('conocio', '1', '0022', 1, '2018-12-03'),
('pregunta1', '5', '0022', 1, '2018-12-03'),
('1', '5', '0022', 1, '2018-12-03'),
('2', '4', '0022', 1, '2018-12-03'),
('3', '3', '0022', 1, '2018-12-03'),
('4', '4', '0022', 1, '2018-12-03'),
('5', '5', '0022', 1, '2018-12-03'),
('1_1', '4', '0022', 1, '2018-12-03'),
('1_2', '4', '0022', 1, '2018-12-03'),
('1_3', '5', '0022', 1, '2018-12-03'),
('1_4', '5', '0022', 1, '2018-12-03'),
('1_5', '4', '0022', 1, '2018-12-03'),
('comentarios', '', '0022', 1, '2018-12-03'),
('comentarios1', '', '0022', 1, '2018-12-03'),
('comentarios2', '', '0022', 1, '2018-12-03'),
('tiempo', '1', '0023', 2, '2018-12-03'),
('conocio', '1', '0023', 2, '2018-12-03'),
('comentarios', '', '0023', 2, '2018-12-03'),
('comentarios1', '', '0023', 2, '2018-12-03'),
('comentarios2', '', '0023', 2, '2018-12-03'),
('tiempo', '1', '0023', 2, '2018-12-03'),
('conocio', '1', '0023', 2, '2018-12-03'),
('comentarios', '', '0023', 2, '2018-12-03'),
('comentarios1', '', '0023', 2, '2018-12-03'),
('comentarios2', '', '0023', 2, '2018-12-03'),
('tiempo', '1', '0023', 2, '2018-12-03'),
('conocio', '1', '0023', 2, '2018-12-03'),
('comentarios', '', '0023', 2, '2018-12-03'),
('comentarios1', '', '0023', 2, '2018-12-03'),
('comentarios2', '', '0023', 2, '2018-12-03'),
('tiempo', '1', '0022', 1, '2018-12-03'),
('conocio', '1', '0022', 1, '2018-12-03'),
('pregunta1', '2', '0022', 1, '2018-12-03'),
('1', '2', '0022', 1, '2018-12-03'),
('2', '2', '0022', 1, '2018-12-03'),
('3', '2', '0022', 1, '2018-12-03'),
('4', '3', '0022', 1, '2018-12-03'),
('5', '4', '0022', 1, '2018-12-03'),
('1_1', '2', '0022', 1, '2018-12-03'),
('1_2', '2', '0022', 1, '2018-12-03'),
('1_3', '2', '0022', 1, '2018-12-03'),
('1_4', '2', '0022', 1, '2018-12-03'),
('1_5', '2', '0022', 1, '2018-12-03'),
('comentarios', '', '0022', 1, '2018-12-03'),
('comentarios1', '', '0022', 1, '2018-12-03'),
('comentarios2', '', '0022', 1, '2018-12-03'),
('tiempo', '1', '0022', 1, '2018-12-03'),
('conocio', '1', '0022', 1, '2018-12-03'),
('pregunta1', '2', '0022', 1, '2018-12-03'),
('1', '3', '0022', 1, '2018-12-03'),
('2', '3', '0022', 1, '2018-12-03'),
('3', '3', '0022', 1, '2018-12-03'),
('4', '4', '0022', 1, '2018-12-03'),
('5', '2', '0022', 1, '2018-12-03'),
('1_1', '2', '0022', 1, '2018-12-03'),
('1_2', '2', '0022', 1, '2018-12-03'),
('1_3', '2', '0022', 1, '2018-12-03'),
('1_4', '4', '0022', 1, '2018-12-03'),
('1_5', '2', '0022', 1, '2018-12-03'),
('comentarios', '', '0022', 1, '2018-12-03'),
('comentarios1', '', '0022', 1, '2018-12-03'),
('comentarios2', '', '0022', 1, '2018-12-03'),
('tiempo', '1', '0022', 1, '2018-12-04'),
('conocio', '1', '0022', 1, '2018-12-04'),
('pregunta1', '4', '0022', 1, '2018-12-04'),
('1', '4', '0022', 1, '2018-12-04'),
('2', '4', '0022', 1, '2018-12-04'),
('3', '4', '0022', 1, '2018-12-04'),
('4', '4', '0022', 1, '2018-12-04'),
('5', '4', '0022', 1, '2018-12-04'),
('1_1', '4', '0022', 1, '2018-12-04'),
('1_2', '4', '0022', 1, '2018-12-04'),
('1_3', '4', '0022', 1, '2018-12-04'),
('1_4', '4', '0022', 1, '2018-12-04'),
('1_5', '4', '0022', 1, '2018-12-04'),
('comentarios', '', '0022', 1, '2018-12-04'),
('comentarios1', '', '0022', 1, '2018-12-04'),
('comentarios2', '', '0022', 1, '2018-12-04'),
('tiempo', '1', '0022', 1, '2018-12-04'),
('conocio', '1', '0022', 1, '2018-12-04'),
('pregunta1', '5', '0022', 1, '2018-12-04'),
('1', '4', '0022', 1, '2018-12-04'),
('2', '5', '0022', 1, '2018-12-04'),
('3', '5', '0022', 1, '2018-12-04'),
('4', '4', '0022', 1, '2018-12-04'),
('5', '5', '0022', 1, '2018-12-04'),
('1_1', '5', '0022', 1, '2018-12-04'),
('1_2', '5', '0022', 1, '2018-12-04'),
('1_3', '4', '0022', 1, '2018-12-04'),
('1_4', '5', '0022', 1, '2018-12-04'),
('1_5', '4', '0022', 1, '2018-12-04'),
('comentarios', '', '0022', 1, '2018-12-04'),
('comentarios1', '', '0022', 1, '2018-12-04'),
('comentarios2', '', '0022', 1, '2018-12-04'),
('tiempo', '1', '0022', 1, '2018-12-04'),
('conocio', '1', '0022', 1, '2018-12-04'),
('pregunta1', '5', '0022', 1, '2018-12-04'),
('1', '4', '0022', 1, '2018-12-04'),
('2', '5', '0022', 1, '2018-12-04'),
('3', '5', '0022', 1, '2018-12-04'),
('4', '4', '0022', 1, '2018-12-04'),
('5', '5', '0022', 1, '2018-12-04'),
('1_1', '5', '0022', 1, '2018-12-04'),
('1_2', '5', '0022', 1, '2018-12-04'),
('1_3', '4', '0022', 1, '2018-12-04'),
('1_4', '5', '0022', 1, '2018-12-04'),
('1_5', '4', '0022', 1, '2018-12-04'),
('comentarios', '', '0022', 1, '2018-12-04'),
('comentarios1', '', '0022', 1, '2018-12-04'),
('comentarios2', '', '0022', 1, '2018-12-04'),
('tiempo', '1', '0022', 1, '2018-12-04'),
('conocio', '1', '0022', 1, '2018-12-04'),
('pregunta1', '5', '0022', 1, '2018-12-04'),
('1', '4', '0022', 1, '2018-12-04'),
('2', '5', '0022', 1, '2018-12-04'),
('3', '5', '0022', 1, '2018-12-04'),
('4', '4', '0022', 1, '2018-12-04'),
('5', '5', '0022', 1, '2018-12-04'),
('1_1', '5', '0022', 1, '2018-12-04'),
('1_2', '5', '0022', 1, '2018-12-04'),
('1_3', '4', '0022', 1, '2018-12-04'),
('1_4', '5', '0022', 1, '2018-12-04'),
('1_5', '4', '0022', 1, '2018-12-04'),
('comentarios', '', '0022', 1, '2018-12-04'),
('comentarios1', '', '0022', 1, '2018-12-04'),
('comentarios2', '', '0022', 1, '2018-12-04'),
('tiempo', '1', '0022', 1, '2018-12-04'),
('conocio', '1', '0022', 1, '2018-12-04'),
('pregunta1', '5', '0022', 1, '2018-12-04'),
('1', '4', '0022', 1, '2018-12-04'),
('2', '5', '0022', 1, '2018-12-04'),
('3', '5', '0022', 1, '2018-12-04'),
('4', '4', '0022', 1, '2018-12-04'),
('5', '5', '0022', 1, '2018-12-04'),
('1_1', '5', '0022', 1, '2018-12-04'),
('1_2', '5', '0022', 1, '2018-12-04'),
('1_3', '4', '0022', 1, '2018-12-04'),
('1_4', '5', '0022', 1, '2018-12-04'),
('1_5', '4', '0022', 1, '2018-12-04'),
('comentarios', '', '0022', 1, '2018-12-04'),
('comentarios1', '', '0022', 1, '2018-12-04'),
('comentarios2', '', '0022', 1, '2018-12-04'),
('tiempo', '1', '0022', 1, '2018-12-04'),
('conocio', '1', '0022', 1, '2018-12-04'),
('pregunta1', '5', '0022', 1, '2018-12-04'),
('1', '4', '0022', 1, '2018-12-04'),
('2', '5', '0022', 1, '2018-12-04'),
('3', '5', '0022', 1, '2018-12-04'),
('4', '4', '0022', 1, '2018-12-04'),
('5', '5', '0022', 1, '2018-12-04'),
('1_1', '5', '0022', 1, '2018-12-04'),
('1_2', '5', '0022', 1, '2018-12-04'),
('1_3', '4', '0022', 1, '2018-12-04'),
('1_4', '5', '0022', 1, '2018-12-04'),
('1_5', '4', '0022', 1, '2018-12-04'),
('comentarios', '', '0022', 1, '2018-12-04'),
('comentarios1', '', '0022', 1, '2018-12-04'),
('comentarios2', '', '0022', 1, '2018-12-04'),
('tiempo', '1', '0022', 1, '2018-12-04'),
('conocio', '1', '0022', 1, '2018-12-04'),
('pregunta1', '5', '0022', 1, '2018-12-04'),
('1', '4', '0022', 1, '2018-12-04'),
('2', '5', '0022', 1, '2018-12-04'),
('3', '5', '0022', 1, '2018-12-04'),
('4', '4', '0022', 1, '2018-12-04'),
('5', '5', '0022', 1, '2018-12-04'),
('1_1', '5', '0022', 1, '2018-12-04'),
('1_2', '5', '0022', 1, '2018-12-04'),
('1_3', '4', '0022', 1, '2018-12-04'),
('1_4', '5', '0022', 1, '2018-12-04'),
('1_5', '4', '0022', 1, '2018-12-04'),
('comentarios', '', '0022', 1, '2018-12-04'),
('comentarios1', '', '0022', 1, '2018-12-04'),
('comentarios2', '', '0022', 1, '2018-12-04'),
('tiempo', '1', '0022', 1, '2018-12-04'),
('conocio', '1', '0022', 1, '2018-12-04'),
('pregunta1', '5', '0022', 1, '2018-12-04'),
('1', '4', '0022', 1, '2018-12-04'),
('2', '5', '0022', 1, '2018-12-04'),
('3', '5', '0022', 1, '2018-12-04'),
('4', '4', '0022', 1, '2018-12-04'),
('5', '5', '0022', 1, '2018-12-04'),
('1_1', '5', '0022', 1, '2018-12-04'),
('1_2', '5', '0022', 1, '2018-12-04'),
('1_3', '4', '0022', 1, '2018-12-04'),
('1_4', '5', '0022', 1, '2018-12-04'),
('1_5', '4', '0022', 1, '2018-12-04'),
('comentarios', '', '0022', 1, '2018-12-04'),
('comentarios1', '', '0022', 1, '2018-12-04'),
('comentarios2', '', '0022', 1, '2018-12-04'),
('tiempo', '1', '0022', 1, '2018-12-04'),
('conocio', '1', '0022', 1, '2018-12-04'),
('pregunta1', '5', '0022', 1, '2018-12-04'),
('1', '4', '0022', 1, '2018-12-04'),
('2', '5', '0022', 1, '2018-12-04'),
('3', '5', '0022', 1, '2018-12-04'),
('4', '4', '0022', 1, '2018-12-04'),
('5', '5', '0022', 1, '2018-12-04'),
('1_1', '5', '0022', 1, '2018-12-04'),
('1_2', '5', '0022', 1, '2018-12-04'),
('1_3', '4', '0022', 1, '2018-12-04'),
('1_4', '5', '0022', 1, '2018-12-04'),
('1_5', '4', '0022', 1, '2018-12-04'),
('comentarios', '', '0022', 1, '2018-12-04'),
('comentarios1', '', '0022', 1, '2018-12-04'),
('comentarios2', '', '0022', 1, '2018-12-04'),
('tiempo', '1', '0022', 1, '2018-12-04'),
('conocio', '1', '0022', 1, '2018-12-04'),
('pregunta1', '5', '0022', 1, '2018-12-04'),
('1', '4', '0022', 1, '2018-12-04'),
('2', '5', '0022', 1, '2018-12-04'),
('3', '5', '0022', 1, '2018-12-04'),
('4', '4', '0022', 1, '2018-12-04'),
('5', '5', '0022', 1, '2018-12-04'),
('1_1', '5', '0022', 1, '2018-12-04'),
('1_2', '5', '0022', 1, '2018-12-04'),
('1_3', '4', '0022', 1, '2018-12-04'),
('1_4', '5', '0022', 1, '2018-12-04'),
('1_5', '4', '0022', 1, '2018-12-04'),
('comentarios', '', '0022', 1, '2018-12-04'),
('comentarios1', '', '0022', 1, '2018-12-04'),
('comentarios2', '', '0022', 1, '2018-12-04'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '1', '0022', 1, '2018-12-18'),
('pregunta1', '4', '0022', 1, '2018-12-18'),
('1', '4', '0022', 1, '2018-12-18'),
('2', '4', '0022', 1, '2018-12-18'),
('3', '4', '0022', 1, '2018-12-18'),
('1_1', '4', '0022', 1, '2018-12-18'),
('1_2', '4', '0022', 1, '2018-12-18'),
('1_3', '4', '0022', 1, '2018-12-18'),
('1_4', '3', '0022', 1, '2018-12-18'),
('apoyo', 'no', '0022', 1, '2018-12-18'),
('comentarios', '', '0022', 1, '2018-12-18'),
('comentarios1', '', '0022', 1, '2018-12-18'),
('comentarios2', '', '0022', 1, '2018-12-18'),
('conocio', '2', '0022', 1, '2018-12-18'),
('tiempo', '4', '0022', 1, '2018-12-18'),
('pregunta1', '4', '0022', 1, '2018-12-18'),
('1', '4', '0022', 1, '2018-12-18'),
('2', '4', '0022', 1, '2018-12-18'),
('3', '4', '0022', 1, '2018-12-18'),
('1_1', '4', '0022', 1, '2018-12-18'),
('1_2', '5', '0022', 1, '2018-12-18'),
('1_3', '4', '0022', 1, '2018-12-18'),
('1_4', '5', '0022', 1, '2018-12-18'),
('apoyo', 'si', '0022', 1, '2018-12-18'),
('comentarios', 'hola 1', '0022', 1, '2018-12-18'),
('comentarios1', 'hola 2', '0022', 1, '2018-12-18'),
('comentarios2', 'hola 3', '0022', 1, '2018-12-18'),
('recomendacion', 'Definitivamente', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '2', '0022', 1, '2018-12-18'),
('pregunta1', '5', '0022', 1, '2018-12-18'),
('1', '3', '0022', 1, '2018-12-18'),
('2', '1', '0022', 1, '2018-12-18'),
('3', '5', '0022', 1, '2018-12-18'),
('1_1', '5', '0022', 1, '2018-12-18'),
('1_2', '4', '0022', 1, '2018-12-18'),
('1_3', '3', '0022', 1, '2018-12-18'),
('1_4', '4', '0022', 1, '2018-12-18'),
('apoyo', 'si', '0022', 1, '2018-12-18'),
('comentarios', 'hdghdh', '0022', 1, '2018-12-18'),
('comentarios1', 'ssss', '0022', 1, '2018-12-18'),
('comentarios2', 'ndff', '0022', 1, '2018-12-18'),
('recomendacion', 'si', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '1', '0022', 1, '2018-12-18'),
('pregunta1', '1', '0022', 1, '2018-12-18'),
('1', '1', '0022', 1, '2018-12-18'),
('2', '1', '0022', 1, '2018-12-18'),
('3', '1', '0022', 1, '2018-12-18'),
('1_1', '1', '0022', 1, '2018-12-18'),
('1_2', '1', '0022', 1, '2018-12-18'),
('1_3', '1', '0022', 1, '2018-12-18'),
('1_4', '1', '0022', 1, '2018-12-18'),
('apoyo', 'si', '0022', 1, '2018-12-18'),
('comentarios', 'ASDASD', '0022', 1, '2018-12-18'),
('comentarios1', '', '0022', 1, '2018-12-18'),
('comentarios2', '', '0022', 1, '2018-12-18'),
('recomendacion', 'Definitivamente', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '1', '0022', 1, '2018-12-18'),
('pregunta1', '3', '0022', 1, '2018-12-18'),
('1', '3', '0022', 1, '2018-12-18'),
('2', '3', '0022', 1, '2018-12-18'),
('3', '3', '0022', 1, '2018-12-18'),
('1_1', '4', '0022', 1, '2018-12-18'),
('1_2', '4', '0022', 1, '2018-12-18'),
('1_3', '5', '0022', 1, '2018-12-18'),
('1_4', '4', '0022', 1, '2018-12-18'),
('apoyo', 'si', '0022', 1, '2018-12-18'),
('comentarios', '', '0022', 1, '2018-12-18'),
('comentarios1', '', '0022', 1, '2018-12-18'),
('comentarios2', '', '0022', 1, '2018-12-18'),
('recomendacion', 'Probablemente', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '1', '0022', 1, '2018-12-18'),
('pregunta1', '3', '0022', 1, '2018-12-18'),
('1', '3', '0022', 1, '2018-12-18'),
('2', '3', '0022', 1, '2018-12-18'),
('3', '3', '0022', 1, '2018-12-18'),
('1_1', '4', '0022', 1, '2018-12-18'),
('1_2', '4', '0022', 1, '2018-12-18'),
('1_3', '5', '0022', 1, '2018-12-18'),
('1_4', '4', '0022', 1, '2018-12-18'),
('apoyo', 'si', '0022', 1, '2018-12-18'),
('comentarios', '', '0022', 1, '2018-12-18'),
('comentarios1', '', '0022', 1, '2018-12-18'),
('comentarios2', '', '0022', 1, '2018-12-18'),
('recomendacion', 'Probablemente', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '1', '0022', 1, '2018-12-18'),
('pregunta1', '3', '0022', 1, '2018-12-18'),
('1', '4', '0022', 1, '2018-12-18'),
('2', '5', '0022', 1, '2018-12-18'),
('3', '4', '0022', 1, '2018-12-18'),
('1_1', '5', '0022', 1, '2018-12-18'),
('1_2', '5', '0022', 1, '2018-12-18'),
('1_3', '5', '0022', 1, '2018-12-18'),
('1_4', '5', '0022', 1, '2018-12-18'),
('apoyo', 'no', '0022', 1, '2018-12-18'),
('comentarios', '', '0022', 1, '2018-12-18'),
('comentarios1', '', '0022', 1, '2018-12-18'),
('comentarios2', '', '0022', 1, '2018-12-18'),
('recomendacion', 'si', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '1', '0022', 1, '2018-12-18'),
('pregunta1', '5', '0022', 1, '2018-12-18'),
('1', '5', '0022', 1, '2018-12-18'),
('2', '5', '0022', 1, '2018-12-18'),
('3', '4', '0022', 1, '2018-12-18'),
('1_1', '4', '0022', 1, '2018-12-18'),
('1_2', '4', '0022', 1, '2018-12-18'),
('1_3', '5', '0022', 1, '2018-12-18'),
('1_4', '4', '0022', 1, '2018-12-18'),
('apoyo', 'no', '0022', 1, '2018-12-18'),
('comentarios', '', '0022', 1, '2018-12-18'),
('comentarios1', '', '0022', 1, '2018-12-18'),
('comentarios2', '', '0022', 1, '2018-12-18'),
('recomendacion', 'Definitivamente', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '1', '0022', 1, '2018-12-18'),
('pregunta1', '5', '0022', 1, '2018-12-18'),
('1', '5', '0022', 1, '2018-12-18'),
('2', '4', '0022', 1, '2018-12-18'),
('3', '5', '0022', 1, '2018-12-18'),
('1_1', '4', '0022', 1, '2018-12-18'),
('1_2', '4', '0022', 1, '2018-12-18'),
('1_3', '5', '0022', 1, '2018-12-18'),
('1_4', '4', '0022', 1, '2018-12-18'),
('apoyo', 'si', '0022', 1, '2018-12-18'),
('comentarios', '', '0022', 1, '2018-12-18'),
('comentarios1', '', '0022', 1, '2018-12-18'),
('comentarios2', '', '0022', 1, '2018-12-18'),
('recomendacion', 'si', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '1', '0022', 1, '2018-12-18'),
('pregunta1', '5', '0022', 1, '2018-12-18'),
('1', '5', '0022', 1, '2018-12-18'),
('2', '4', '0022', 1, '2018-12-18'),
('3', '5', '0022', 1, '2018-12-18'),
('1_1', '4', '0022', 1, '2018-12-18'),
('1_2', '4', '0022', 1, '2018-12-18'),
('1_3', '5', '0022', 1, '2018-12-18'),
('1_4', '4', '0022', 1, '2018-12-18'),
('apoyo', 'si', '0022', 1, '2018-12-18'),
('comentarios', '', '0022', 1, '2018-12-18'),
('comentarios1', '', '0022', 1, '2018-12-18'),
('comentarios2', '', '0022', 1, '2018-12-18'),
('recomendacion', 'si', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '1', '0022', 1, '2018-12-18'),
('pregunta1', '5', '0022', 1, '2018-12-18'),
('1', '5', '0022', 1, '2018-12-18'),
('2', '4', '0022', 1, '2018-12-18'),
('3', '5', '0022', 1, '2018-12-18'),
('1_1', '4', '0022', 1, '2018-12-18'),
('1_2', '4', '0022', 1, '2018-12-18'),
('1_3', '5', '0022', 1, '2018-12-18'),
('1_4', '4', '0022', 1, '2018-12-18'),
('apoyo', 'si', '0022', 1, '2018-12-18'),
('comentarios', '', '0022', 1, '2018-12-18'),
('comentarios1', '', '0022', 1, '2018-12-18'),
('comentarios2', '', '0022', 1, '2018-12-18'),
('recomendacion', 'si', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '1', '0022', 1, '2018-12-18'),
('pregunta1', '5', '0022', 1, '2018-12-18'),
('1', '5', '0022', 1, '2018-12-18'),
('2', '4', '0022', 1, '2018-12-18'),
('3', '5', '0022', 1, '2018-12-18'),
('1_1', '4', '0022', 1, '2018-12-18'),
('1_2', '4', '0022', 1, '2018-12-18'),
('1_3', '5', '0022', 1, '2018-12-18'),
('1_4', '4', '0022', 1, '2018-12-18'),
('apoyo', 'si', '0022', 1, '2018-12-18'),
('comentarios', '', '0022', 1, '2018-12-18'),
('comentarios1', '', '0022', 1, '2018-12-18'),
('comentarios2', '', '0022', 1, '2018-12-18'),
('recomendacion', 'si', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '1', '0022', 1, '2018-12-18'),
('pregunta1', '5', '0022', 1, '2018-12-18'),
('1', '5', '0022', 1, '2018-12-18'),
('2', '4', '0022', 1, '2018-12-18'),
('3', '5', '0022', 1, '2018-12-18'),
('1_1', '4', '0022', 1, '2018-12-18'),
('1_2', '4', '0022', 1, '2018-12-18'),
('1_3', '5', '0022', 1, '2018-12-18'),
('1_4', '4', '0022', 1, '2018-12-18'),
('apoyo', 'si', '0022', 1, '2018-12-18'),
('comentarios', '', '0022', 1, '2018-12-18'),
('comentarios1', '', '0022', 1, '2018-12-18'),
('comentarios2', '', '0022', 1, '2018-12-18'),
('recomendacion', 'si', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '1', '0022', 1, '2018-12-18'),
('pregunta1', '5', '0022', 1, '2018-12-18'),
('1', '5', '0022', 1, '2018-12-18'),
('2', '4', '0022', 1, '2018-12-18'),
('3', '5', '0022', 1, '2018-12-18'),
('1_1', '4', '0022', 1, '2018-12-18'),
('1_2', '4', '0022', 1, '2018-12-18'),
('1_3', '5', '0022', 1, '2018-12-18'),
('1_4', '4', '0022', 1, '2018-12-18'),
('apoyo', 'si', '0022', 1, '2018-12-18'),
('comentarios', '', '0022', 1, '2018-12-18'),
('comentarios1', '', '0022', 1, '2018-12-18'),
('comentarios2', '', '0022', 1, '2018-12-18'),
('recomendacion', 'si', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '1', '0022', 1, '2018-12-18'),
('pregunta1', '5', '0022', 1, '2018-12-18'),
('1', '5', '0022', 1, '2018-12-18'),
('2', '4', '0022', 1, '2018-12-18'),
('3', '5', '0022', 1, '2018-12-18'),
('1_1', '4', '0022', 1, '2018-12-18'),
('1_2', '4', '0022', 1, '2018-12-18'),
('1_3', '5', '0022', 1, '2018-12-18'),
('1_4', '4', '0022', 1, '2018-12-18'),
('apoyo', 'si', '0022', 1, '2018-12-18'),
('comentarios', '', '0022', 1, '2018-12-18'),
('comentarios1', '', '0022', 1, '2018-12-18'),
('comentarios2', '', '0022', 1, '2018-12-18'),
('recomendacion', 'si', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-18'),
('tiempo', '1', '0022', 1, '2018-12-18'),
('pregunta1', '3', '0022', 1, '2018-12-18'),
('1', '2', '0022', 1, '2018-12-18'),
('2', '3', '0022', 1, '2018-12-18'),
('3', '4', '0022', 1, '2018-12-18'),
('1_1', '3', '0022', 1, '2018-12-18'),
('1_2', '4', '0022', 1, '2018-12-18'),
('1_3', '4', '0022', 1, '2018-12-18'),
('1_4', '3', '0022', 1, '2018-12-18'),
('apoyo', 'si', '0022', 1, '2018-12-18'),
('comentarios', '', '0022', 1, '2018-12-18'),
('comentarios1', '', '0022', 1, '2018-12-18'),
('comentarios2', '', '0022', 1, '2018-12-18'),
('recomendacion', 'Probablemente', '0022', 1, '2018-12-18'),
('conocio', '1', '0022', 1, '2018-12-19'),
('tiempo', '1', '0022', 1, '2018-12-19'),
('pregunta1', '1', '0022', 1, '2018-12-19'),
('1', '5', '0022', 1, '2018-12-19'),
('2', '5', '0022', 1, '2018-12-19'),
('3', '5', '0022', 1, '2018-12-19'),
('1_1', '2', '0022', 1, '2018-12-19'),
('1_2', '2', '0022', 1, '2018-12-19'),
('1_3', '2', '0022', 1, '2018-12-19'),
('1_4', '2', '0022', 1, '2018-12-19'),
('apoyo', 'no', '0022', 1, '2018-12-19'),
('comentarios', '', '0022', 1, '2018-12-19'),
('comentarios1', '', '0022', 1, '2018-12-19'),
('comentarios2', '', '0022', 1, '2018-12-19'),
('recomendacion', 'Definitivamente', '0022', 1, '2018-12-19'),
('conocio', '1', '0022', 1, '2018-12-19'),
('tiempo', '3', '0022', 1, '2018-12-19'),
('pregunta1', '4', '0022', 1, '2018-12-19'),
('1', '4', '0022', 1, '2018-12-19'),
('2', '4', '0022', 1, '2018-12-19'),
('3', '4', '0022', 1, '2018-12-19'),
('1_1', '4', '0022', 1, '2018-12-19'),
('1_2', '4', '0022', 1, '2018-12-19'),
('1_3', '4', '0022', 1, '2018-12-19'),
('1_4', '4', '0022', 1, '2018-12-19'),
('apoyo', 'si', '0022', 1, '2018-12-19'),
('comentarios', '', '0022', 1, '2018-12-19'),
('comentarios1', '', '0022', 1, '2018-12-19'),
('comentarios2', '', '0022', 1, '2018-12-19'),
('recomendacion', 'Definitivamente', '0022', 1, '2018-12-19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entero`
--

CREATE TABLE `entero` (
  `id_entero` int(2) NOT NULL,
  `entero` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `entero`
--

INSERT INTO `entero` (`id_entero`, `entero`) VALUES
(1, 'Amigos, colegas o contactos'),
(2, 'TV'),
(3, 'Radio'),
(4, 'Internet'),
(5, 'Redes sociales'),
(6, 'Otros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas2`
--

CREATE TABLE `preguntas2` (
  `id_pregunta2` int(4) NOT NULL,
  `pregunta` varchar(200) NOT NULL,
  `departamento` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `preguntas2`
--

INSERT INTO `preguntas2` (`id_pregunta2`, `pregunta`, `departamento`) VALUES
(1, 'Calidad en el servicio', 'Enlace'),
(2, 'Tiempo de respuesta', 'Enlace'),
(3, 'Atención al Cliente', 'Enlace');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas10`
--

CREATE TABLE `preguntas10` (
  `id_pregunta10` int(2) NOT NULL,
  `id_secundario` decimal(2,1) NOT NULL,
  `pregunta` varchar(100) NOT NULL,
  `departamento` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `preguntas10`
--

INSERT INTO `preguntas10` (`id_pregunta10`, `id_secundario`, `pregunta`, `departamento`) VALUES
(1, '1.1', 'Nóminas  (ISR - SAE)', 'Operaciones'),
(2, '1.2', 'Infonavit - IMSS', 'Contabilidad'),
(3, '1.3', 'Temas jurídicos laborales', 'Contabilidad'),
(4, '1.4', 'Facturación (IVA)', 'Contabilidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(2) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `clave` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre`, `clave`) VALUES
(1, 'Omar Paulino', 'Mabel1q1'),
(2, 'admin', 'admin2018');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `antiguedad`
--
ALTER TABLE `antiguedad`
  ADD PRIMARY KEY (`id_antiguedad`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id_departamento`),
  ADD UNIQUE KEY `departamento` (`departamento`);

--
-- Indices de la tabla `ejecutivos`
--
ALTER TABLE `ejecutivos`
  ADD PRIMARY KEY (`id_ejecutivo`);

--
-- Indices de la tabla `entero`
--
ALTER TABLE `entero`
  ADD PRIMARY KEY (`id_entero`);

--
-- Indices de la tabla `preguntas2`
--
ALTER TABLE `preguntas2`
  ADD PRIMARY KEY (`id_pregunta2`);

--
-- Indices de la tabla `preguntas10`
--
ALTER TABLE `preguntas10`
  ADD PRIMARY KEY (`id_pregunta10`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `antiguedad`
--
ALTER TABLE `antiguedad`
  MODIFY `id_antiguedad` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id_departamento` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `ejecutivos`
--
ALTER TABLE `ejecutivos`
  MODIFY `id_ejecutivo` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `entero`
--
ALTER TABLE `entero`
  MODIFY `id_entero` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `preguntas2`
--
ALTER TABLE `preguntas2`
  MODIFY `id_pregunta2` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `preguntas10`
--
ALTER TABLE `preguntas10`
  MODIFY `id_pregunta10` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
